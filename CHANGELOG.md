# os-agnostic-utils change log
* 2023-07-27, Diagnosed timing bug in os_agnostic_utils, OSProcesses::forkIndirect
    * On Windows, the updates to the launcher script for WSL to work broke some code called by forkIndirect to locate the actual pid of the forked process running the actual bash command
    * The above manifested itself when we had a quickly finishing command, as we could never get a pid for it and the msgfile would never be created (it just hung)
    * To address, the method for locating the pid in the forked process was updated.  Also, commands now need to have a fixed 5second startup delay that allows for the pid to be properly located in the forked process running the actual bash command
    * The update was tested on both Windows and Redhat.
    * Changes were also made in mcmcInterface
