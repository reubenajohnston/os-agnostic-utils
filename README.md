# os-agnostic-utils
**Warning: os-agnostic-utils is in the pre-release stage and is still being refined**

OS agnostic platform with a Python API that supports:
* Shell commands (running, spawning, querying status, or terminating running shell command)
* Process manipulation (forking a new process, querying process status, or terminating a process)
* (In work) Run a MATLAB command or a sequence of commands via Python engine for MATLAB (a.k.a., MATLAB engine)
* (In work) Spawn a set of MATLAB engines to run MATLAB commands in parallel and query their status

# Dependencies
* Python 3 and virtualenv package should be installed first

# Installation
* Create a virtualenv `~/venv/os_agnostic_utils` and activate it
* In your virtualenv
    * Install MATLAB engine for Python.  See the MATLAB instructions for installation.
    * Install the following pip3 packages
        * Windows: psutil jsonpickle pymsgbox pyreadline setproctitle pydevd **pywin32**
        * Linux: psutil jsonpickle pymsgbox pyreadline setproctitle pydevd **posix_ipc sysv_ipc**
* To facilitate editing, install os-agnostic-utils into your virtualenv using:
    * Windows
        `PS C:\> pip3.exe install -e .`
	* Linux
        `$ pip3 install -e .`
* To use the base version, remove the `-e` option from the above command
* Setup os_agnostic_utils/config.xml with appropriate paths
  
# Test
* Windows, in your virtualenv
    `PS C:\> python.exe .\os_agnostic_utils\test\test.py`
* Linux, in your virtualenv
    `$ export PYDEVD_USE_CYTHON=NO && export PYDEVD_USE_FRAME_EVAL=NO && python3 os_agnostic_utils/test/test.py`
  
# Usage
* Windows, in your virtualenv
    `C:\> python.exe -m os_agnostic_utils RUNBASHCMD --shellTitle "Hello neo..." --msgFile C:\sandboxes\<USERNAME>\mcmcbayes\os-agnostic-utils\demo-status.txt --shellRunView Foreground --shellLog /mnt/c/sandboxes/<USERNAME>/mcmcbayes/os-agnostic-utils/runbashcmd-shelllog.txt --shellCmd /mnt/c/sandboxes/<USERNAME>/mcmcbayes/os-agnostic-utils/scripts/demo --shellArgs 'Howdy ho!' 5`
* Linux, in your virtualenv
    `$ python3 -m os_agnostic_utils RUNBASHCMD --msgFile /sandboxes/<USERNAME>/mcmcbayes/os-agnostic-utils/demo-status.txt --debug --shellTitle "Run demo" --shellLog /sandboxes/<USERNAME>/mcmcbayes/os-agnostic-utils/runbashcmd-shelllog.txt --shellCmd /sandboxes/<USERNAME>/mcmcbayes/os-agnostic-utils/scripts/demo --shellArgs \'Howdy ho\!\' 5`
* To run the regression tests, just run the test/test.py file (config.xml needs to be configured properly)
  
# Remove
* Delete virtualenv `~/venv/os_agnostic_utils`
  
# Documentation

## API
config.xml: The parameters "shellStartScript", "terminal", "shell", and "python" are all located in the configuration file and should be setup specific to the operating system.  These parameters are used by os-agnostic-utils to invoke "terminal+shell" in a way that is consistent across different operating systems.  
* Windows, "shellStartScript" will be startBashWSL.cmd, "terminal" will be wsl.exe, and "shell" (bash in wsl) will be /usr/bin/bash and startBashWSL.cmd will start a new wsl.exe terminal that runs commands in a bash shell
* Mac, "shellStartScript" will be startBashMacOs, "terminal" will be alacritty, and "shell" (bash) will be /bin/bash and startBashMacOs will start a new alacritty terminal that runs commands in a bash shell
* Linux, "shellStartScript" will be startBashUbuntuOs, "terminal" will be gnome-terminal, and "shell" (bash) will be /bin/bash and startBashUbuntuOs will start a new gnome-terminal that runs commands in a bash shell

The following commands are supported by the top-level API.
* RUNBASHCMD: This function will fork a new process using "shellStartScript" to start a new "terminal" and in it, invoke "shellCmd" with "shellArgs" running in "shell".  A msgFile is created and the initial ##STATE## in it will be set to "Running".  The function then waits for the process to complete and on completion will update ##STATE## in msgFile to "Finished".
* SPAWNBASHCMD(msgFile,): This function will fork a new process using "shellStartScript" to start a new "terminal" and in it, invoke "shellCmd" with "shellArgs" running in "shell".  A msgFile is created and the initial ##STATE## in it will be set to "Running".  Unlike RUNBASHCMD, this command does not wait for the forked process to complete.
* QUERYBASHCMDSTATUS(msgFile,processPID): This function will check whether the process identified by processPID exists.  If so, ##STATE## in msgFile remains "Running" and this function returns;  otherwise, it will open msgFile to update the ##STATE## and ##EXITCODE## lines properly.
* KILLPID: This function will kill the process with PID="processPID".
* KILLPIDS: This function will kill the process with PID="processParentPID" and any of its children.
* RUNMATLABENGINE: todo
* SPAWNMATLABENGINES: todo
* QUERYMATLABENGINESSTATUS: todo

## Supporting modules

### OSFiles
Simple utility module to perform a few basic file operations.
* queryFile
* createFile
* removeFile

### OSPath
Simple utility module to convert path styles.
* toPosix-Converts a Windows path to Posix style
* toWindows-Converts a Posix path to Windows style
* TODO: toWSLWindows

### OSShell
Simple utility module to launch commands in bash or query their status.
* runBashCommand
* spawnBashCommand
* queryBashCommand

### OSType
Simple utility module to detect the OS type.
* osType-Returns the OsType

### OSXml
Simple utility module for reading the config.xml file.
* parseXML-Helper utility function that returns contents from xmlfile accessible as a list

## Supporting classes

### OSComms
Utility class for communicating.
* setup
* teardown
* speak
* listen
* run

### OSSemaphore
Utility class for named semaphores that are used to monitor a pool of some resource being accessed by multiple processes and perform synchronized updates for the availability of this resource.  Presently supports both Windows (using pywin32 extensions) and Linux (using the python posix_ipc package).  There should be one CREATOR process and one or more USER processes.
* setup (creator or user)-constructs or opens the named pipe; user blocks
* teardown-destroys or closes the named semaphore
* acquire-acquires a semaphore (decrements count) if available or times out
* release-releases a semaphore (increments count) if not max

### OSPipe
Utility class for named pipes that are used to communicate between a parent process and its children.  Presently supports both Windows (using pywin32 extensions) and Linux (using the python posix_ipc package).  There should be one LISTENER process and one or more SPEAKER processes.
* setup()-constructs or opens the named pipe; blocks
* teardown()-destroys or closes the named pipe
* check()-checks for existence of a client connection to a pipe (Role.LISTENER) or a pipe (Role.SPEAKER)
* peek()-checks for existence of message (for LISTENERs only) and returns size of available bytes
* speak(value)-speaks (writes) the bytes in value to the named pipe
* listen(size)-listens (reads) size bytes from the named pipe

### OSProcesses
* fork(function, args)-Traditional fork concept where the current process (parent) creates a new process (child), runs Python function with args only in the child, and has the parent return the pid of the child.  On PosixOS, uses os.fork() and on WindowsOS uses multiprocessing.Process (with spawn method)
* forkIndirect(command, args, proclocatestr)-Alternate fork concept where the current process (parent) creates a new process (child), runs an OS command with args only in the child, and has the parent return the pid of the child.  Uses subprocess.Popen for all instances
* poll(pid,timeout)-Tests whether pid is still running after specified timeout.  Response is either raised TimeoutError after sleeping (pid exists and is running) or simply returns (pid does not exist and has finished)
* getpid(procname)-Finds the child by looking for procname in cmdline and retrieves its pid (necessary for locating specific spawned child of procname)
* killPID(pid)-Kills the process with pid
* killPIDs(parentPid)-Kills the process with parentPid and all its children
* psNameExists(processName)-Determines whether a process named processName is running
