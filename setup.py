#Install:
#$ pip3 install --user -e .
#Test:
#$ export PYDEVD_USE_CYTHON=NO && export PYDEVD_USE_FRAME_EVAL=NO && python3 os_agnostic_utils/test/test.py
#Remove:
#$ pip3 uninstall os-agnostic-utils

from setuptools import setup, find_packages

setup(name='os-agnostic-utils',
      version='0.1',
      description='Operating system agnostic utilities in Python',
      url='https://gitlab.com/reubenajohnston/os-agnostic-utils',
      author='Reuben Johnston',
      author_email='reuben@reubenjohnston.com',
      license='MIT',
      packages=['os_agnostic_utils', 'os_agnostic_utils.test',
                'os_agnostic_utils.os_semaphore', 'os_agnostic_utils.os_semaphore.test',
                'os_agnostic_utils.os_pipe', 'os_agnostic_utils.os_pipe.test',
                'os_agnostic_utils.os_processes', 'os_agnostic_utils.os_processes.test'],
      zip_safe=False)
