#!/usr/bin/env bash

#help:
#$ ./startBashUbuntuOs -h

#Requires gnome-terminal

#https://www.shellscript.sh/tips/getopts/

function usage {
  echo Usage:
  echo     startBash [options] [parameters]
  echo
  echo Required settings:
  echo     -r \<TERMINAL\>'	'uses the specified terminal
  echo     -s \<SHELL\>'	'specifies the path to bash shell
  echo     -c \<CMD\>'	'runs the specified command
  echo
  echo Options:
  echo     -t \<TITLE\>'	'sets the window title
  echo     -h'		'help
  echo     -l \<LOG\>'	'sets the log file
  echo     -b'		'runs in background
  echo
  echo All parameters following valid options are passed as parameters to CMD.  Any strings passed for \<TITLE\> must use single quotes.  Any strings passed as parameters to \<CMD\> must be in single quotes prefixed with escapes.
  echo
  echo E.g., \$ ./startBashUbuntuOs -r /usr/bin/gnome-terminal -s /bin/bash -t \'test title\' -l ~/mylogfile.txt -c scripts/demo \\\'test echo\\\' 10
  echo would run the following:
  echo '      '\$ /usr/bin/gnome-terminal --title \"test title\" -- /bin/bash --login -c \"scripts/demo \'test echo\' 10 2\>\&1 \| tee -a /Users/student/mylogfile.txt\"
  echo that is running the following bash command in gnome-terminal:
  echo '      '\$ /bin/bash --login -c \"scripts/demo \'test echo\' 10 2\>\&1 \| tee -a ~/mylogfile.txt\"
}

ALLARGS=("$@")
NUMARGS=${#ALLARGS[@]}
#echo "NUMARGS=$NUMARGS"

SHELL=""
TERMINAL=""
CMD=""
TITLE=""
LOG=""
BG=$((0))
PARAMS=()

i=0
lastplusi=0
while getopts hr:s:t:l:c:b FLAG; do
  plusi=$(($OPTIND-1)) #plusi is a variable for the next index
  case $FLAG in
    h) #help 
      usage
      exit
      ;;
    r) #terminal
      TERMINAL=$OPTARG
      # "TERMINAL=\"TERMINAL\""
      if [ "$plusi" -gt "$lastplusi" ]; then
        lastplusi=$plusi
      fi
      ;;
    s) #shell
      SHELL=$OPTARG
      # "SHELL=\"SHELL\""
      if [ "$plusi" -gt "$lastplusi" ]; then
        lastplusi=$plusi
      fi
      ;;
    t) #title
      TITLE=$OPTARG
      # "TITLE=\"$TITLE\""
      if [ "$plusi" -gt "$lastplusi" ]; then
        lastplusi=$plusi
      fi
      ;;
    l) #log
      LOG=$OPTARG
      #echo "LOG=\"$LOG\""
      if [ "$plusi" -gt "$lastplusi" ]; then
        lastplusi=$plusi
       fi
      ;;
    c) #cmd
      CMD=$OPTARG
      #echo "CMD=\"$CMD\""
      if [ "$plusi" -gt "$lastplusi" ]; then
        lastplusi=$plusi
      fi
      ;;      
    b) #bg
      BG=$((1))
      #echo "BG=$BG"
      if [ "$plusi" -gt "$lastplusi" ]; then
        lastplusi=$plusi
      fi
      ;;
  esac
  i=$((i+1)) #increment it by 1
done

PARAMSStr=""
for i in `seq $lastplusi 1 $(($NUMARGS-1))`;
do
  PARAMSStr="$PARAMSStr ${ALLARGS[i]}"
done
#echo "PARAMSStr=$PARAMSStr"

if [ -z "$LOG" ]; then
  # True if the string "$LOG" is null (an empty string)
  if [ "$BG" -eq $((1)) ]; then
    #terminal does not support background execution, must run only the shell
    #https://cnuge.github.io/post/nohup_post
    EVALStr="nohup $SHELL --login -c \"$CMD $PARAMSStr\" &"
  else
    EVALStr="$TERMINAL --title \"$TITLE\" -- $SHELL --login -c \"$CMD $PARAMSStr\""  
  fi
else
  if [ "$BG" -eq $((1)) ]; then
    #terminal does not support background execution, must run only the shell
    EVALStr="nohup $SHELL --login -c \"$CMD $PARAMSStr 2>&1 | tee -a "$LOG"\" &"
  else
    EVALStr="$TERMINAL --title \"$TITLE\" -- $SHELL --login -c \"$CMD $PARAMSStr 2>&1 | tee -a "$LOG"\""
  fi
fi
echo "Executing: $EVALStr"
eval $EVALStr