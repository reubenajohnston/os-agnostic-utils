@echo off
rem This batch file enables having a consistent means to spawn and log programs from bash via MATLAB running on Windows, Linux, or Mac OS.  Specifically, this version is for Windows and will spawn a command with parameters in the msys2 shell and setup mingw tools appropriately before running it.  

rem Use EnableDelayedExpansion for generating array-like constructs
setlocal EnableDelayedExpansion

echo MSYS2PATH=%MSYS2PATH%
echo MINGW=%MINGW%

rem set MSYS2PATH=C:\opt\msys64
rem set MINGW=mingw64
rem C:\sandbox\mcmcBayes\code\scripts\startBashWinOs.cmd -r C:\opt\msys64\msys2.exe C: -s C:\opt\msys64\usr\bin\bash.exe -t "Bash window title" -l /C/sandbox/mcmcBayes/logs/demo-log.txt -c /C/sandbox/mcmcBayes/code/scripts/demo 'hello world' 10
rem running from the command line, strings in the arguments must be in ticks (e.g., 'hello world')
rem running from Python, strings in the arguments must be in escaped-ticks (e.g., '\'hello world\'')

rem Capture all the arguments into an array-like construct
set ALL_ARGS=""
set /A i=0
for %%x in (%*) do (
  set /A i=!i!+1
  set ALL_ARGS[!i!]=%%x
)
set /A lastidx=!i!
rem echo There are !lastidx! variables present

rem example method for iterating through the arguments array
for /L %%f in (1,1,!lastidx!) do (
  echo %%f:!ALL_ARGS[%%f]!
)

if "%MSYS2PATH%"=="" (
  call :printhelp
  rem ERROR_ENVVAR_NOT_FOUND=203
  echo ERROR_ENVVAR_NOT_FOUND: MSYS2PATH missing
  exit /b 203
) else (
  if "%MINGW%"=="" (
    call :printhelp
    rem ERROR_ENVVAR_NOT_FOUND=203
    echo ERROR_ENVVAR_NOT_FOUND: MINGW missing
    exit /b 203
  ) else (
	rem environment variables are present
  )
)

rem Variables for required settings
set msyspath=%MSYS2PATH%
set mingw=%MINGW%
set shell=""
set terminal=""
set bashcmd=""

rem Variables for optional settings
set title=""
set logfile=""
set bg=False

rem Collect the required and optional settings
rem fi is a variable for %%f+1
set /A fi=1
for /L %%f in (1,1,!lastidx!) do (
  set /A fi=!fi!+1
  rem ignore the entry unless it starts with -
  if "!ALL_ARGS[%%f]:~0,1!" == "-" (
    rem echo %%f: !ALL_ARGS[%%f]!
    if "!ALL_ARGS[%%f]!" == "-t" ( 
	  set /A lastfi=!fi!
      call set title=%%ALL_ARGS[!lastfi!]%%
    ) else (
	  if "!ALL_ARGS[%%f]!" == "-l" ( 
	    set /A lastfi=!fi!
        call set logfile=%%ALL_ARGS[!lastfi!]%%
      ) else (
	    if "!ALL_ARGS[%%f]!" == "-b" ( 
	      set /A lastfi=!fi!
          call set bg=True
        ) else (
		  if "!ALL_ARGS[%%f]!" == "-c" ( 
	        set /A lastfi=!fi!
            call set bashcmd=%%ALL_ARGS[!lastfi!]%%
          ) else (			
			if "!ALL_ARGS[%%f]!" == "-r" ( 
	          set /A lastfi=!fi!
              call set terminal=%%ALL_ARGS[!lastfi!]%%
            ) else (			
			  if "!ALL_ARGS[%%f]!" == "-s" ( 
				set /A lastfi=!fi!
				call set shell=%%ALL_ARGS[!lastfi!]%%
			  ) else (			
			    if "!ALL_ARGS[%%f]!" == "-r" ( 
	              set /A lastfi=!fi!
                  call set terminal=%%ALL_ARGS[!lastfi!]%%
                ) else (
			      rem It is probably a parameter 				
			    )
			  )
		    )
          )		
        )
	  )
	)
  ) else (
    rem the entry did not start with - and is probably an argument to bashcmd
	rem echo %%f: !ALL_ARGS[%%f]!
  )
)

rem Collect remaining command line arguments to be passed to bashcmd
set /A firstidx=!lastfi!+1
rem echo !firstidx!:!lastidx!
for /L %%f in (!firstidx!,1,!lastidx!) do ( 
  if "!firstidx!"=="!lastidx!" (
    set BASHCMD_ARGS=!ALL_ARGS[%%f]!
  ) else (
    if "%%f"=="!firstidx!" (
      set BASHCMD_ARGS=!ALL_ARGS[%%f]!
    ) else (
      rem add space after
      set BASHCMD_ARGS=!BASHCMD_ARGS! !ALL_ARGS[%%f]!
    )
  )
)

if "%terminal%"=="" (
  call :printhelp
  rem ERROR_INVALID_PARAMETER=87
  echo ERROR_INVALID_PARAMETER: -r missing
  exit /b 87
) else (
  if "%shell%"=="" (
    call :printhelp
    rem ERROR_INVALID_PARAMETER=87
    echo ERROR_INVALID_PARAMETER: -s missing
    exit /b 87
  ) else (
	if %bashcmd%=="" (
      call :printhelp
      rem ERROR_INVALID_PARAMETER=87
	  echo ERROR_INVALID_PARAMETER: -c missing
      rem exit /b 87	  
	)
  )
  )
)

rem set default title if not present
if %title%=="" (
  set title="Start bash"
)
rem set default bg mode if not present
if %bg%=="" (
  set bg=False
)
echo Using:
echo   msyspath=%MSYS2PATH%
echo   mingw=%MINGW%
echo   shell=%shell%
echo   terminal=%terminal%
echo   bashcmd=%bashcmd%
echo   args=!BASHCMD_ARGS!
echo   title=%title%
echo   bg=%bg%

rem set MSYSTEM environment variable to specify appropriate mingw gcc environment in msys2
if %mingw%==mingw64 (
  set "MSYSTEM=MINGW64"
) else (
  if %mingw%==mingw32 (
    set "MSYSTEM=MINGW32"
  ) else (
    call :printhelp
    rem ERROR_INVALID_PARAMETER=87
    exit /b 87	  
  )
)

rem remove quotes from title (if they are present)
for /f "useback tokens=*" %%a in ('%title%') do set title=%%~a

:startbash
echo ..................................................................
rem See the usage at bottom of this file for escape character rules
if %logfile%=="" (
  if %bg%==False (
    echo start "%title%" /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
    echo ..................................................................
    start "%title%" /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
  ) else (
    echo start "%title%" /B /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
    echo ..................................................................
    start "%title%" /B /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
  )
) else (
  if %bg%==False (
    echo start "%title%" /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
    echo ..................................................................
    start "%title%" /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
  ) else (
    echo start "%title%" /B /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
    echo ..................................................................
    start "%title%" /B /D %msyspath% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
  )
)
echo ..................................................................
exit /b %ERRORLEVEL%

:printhelp
echo Usage:
echo     %~1 [options] [parameters for cmd]
echo.
echo Required settings:
echo     --msys2^=^<MSYS2^>        Full path to msys2 main directory
echo     --mingw32 ^| --mingw64  Specifies which gcc (32- or 64-bit) to start bash using
echo     --bash^=^<BASH^>          Full path to bash.exe
echo     --cmd^=^<CMD^>            Full posix path for command to execute in bash
echo.
echo Options:
echo     --help                 Display this help and exit
echo     --title^=^<TITLE^>        Title for the command prompt window invoked (if --bg is absent)
echo     --log^=^<LOG^>            Full posix path for logging stdout and stderr
echo     --bg                   Run in background (i.e., no window visible)
echo.
echo All parameters following valid options are passed as parameters to CMD.
echo.
echo E.g., C:\^> startBash.cmd --title "Bash" --msys2 C:\msys64 --mingw64 --log /C/mylogfile.txt \
echo            --bash C:\msys64\usr\bin\bash.exe --cmd /C/myscript arg1 arg2 arg3
echo would set an environment variable MSYSTEM to MINGW64 (due to --mingw64) and then run the
echo following from a Windows command shell:
echo       C:\^> start "Bash" /D C:\msys64 C:\msys64\usr\bin\bash.exe --login -c \
echo            "/c/myscript arg1 arg2 arg3 2>&1 | tee -a /C/mylogfile.txt"
echo.
echo In the case --bg is set, /B would be added as an option to the start, ^'internal^' command
echo to the Windows command shell.
echo.
echo Cautions to consider:
echo Parameters for cmd must not conflict with names of any required or optional parameters.
echo Parameters for cmd with command file special characters (for the Windows command shell) 
echo   present must be escaped.  E.g., per https://www.robvanderwoude.com/escapechars.php, the 
echo   following escapes are necessary:
echo     %% must be escaped via "%%%%"
echo     ^^ must be escaped via "^^"
echo     ^& must be escaped via "^&"
echo     ^< and ^> must be escaped via "^<" and "^>"
echo     ^| must be escaped via "^|"
echo     ^^! must be escaped via "^^!"
exit /b 0

endlocal

rem Usage:
rem C:\> C:\sandbox\mcmcBayes\code\scripts\startBash.cmd --help
rem   this: C:\> C:\sandbox\mcmcBayes\code\scripts\startBash.cmd --title=Demo --msys2=C:\opt\msys64 --mingw64 --log=/C/sandbox/mcmcBayes/logs/demoLog.txt --bash=C:\opt\msys64\usr\bin\bash.exe --cmd=/C/sandbox/mcmcBayes/code/scripts/demo \"reub is cool^^^!\"
rem   runs: C:\> set "MSYSTEM=MINGW64" & start "Demo" /D C:\opt\msys64 C:\opt\msys64\usr\bin\bash.exe --login -c "/C/sandbox/mcmcBayes/code/scripts/demo \"reub is cool^^^!\" 2>&1 | tee -a /C/sandbox/mcmcBayes/logs/demoLog.txt"
rem   alternately, via mcmcbayes_utils_command_parser.py: C:\> C:\opt\Python36\python.exe C:\sandbox\mcmcBayes\code\python\mcmcbayes_utils_command_parser.py RUNBASHCMD --shellPath "C:\sandbox\mcmcBayes\code\scripts\startBash.cmd --title=\"Hello\" --msys2=C:\opt\msys64 --mingw64 --bash=C:\opt\msys64\usr\bin\bash.exe" --killFile C:\sandbox\mcmcBayes\temp\aLittleBitOfPoison --UUIDMsgFile C:\sandbox\mcmcBayes\temp\demo-bashCmdStatus.txt --shellCmdRunView mcmcBayes.t_runView.Foreground --shellCmdLog /C/sandbox/mcmcBayes/logs/demo-log.txt --shellCmd /C/sandbox/mcmcBayes/code/scripts/demo --shellArgs \"hello neo...\"

rem Notes
rem
rem example method for iterating through the arguments array
rem for /L %%f in (1,1,!lastidx!) do (
rem   echo %%f:!ALL_ARGS[%%f]!
rem )

rem preferred example method for iterating through the arguments array
rem set /A i=0
rem for /L %%f in (1,1,!lastidx!) do (
rem   set /A i=!i!+1
rem   call echo !i!:%%ALL_ARGS[!i!]%%
rem )
