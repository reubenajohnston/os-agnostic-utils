@echo off
rem This batch file enables having a consistent means to spawn and log programs from bash via MATLAB running on Windows, Linux, or Mac OS.  Specifically, this version is for Windows and will spawn a bash shell + command + bash parameters that executes in the wsl ubuntu terminal.  

rem Argument list:
rem     -t: title for the window
rem     -l: logfile
rem     -b: if present, runs command in background (headless)
rem     -r: terminal (e.g., WSL Ubuntu app)
rem     -s: shell to run in terminal (e.g., bash)
rem     -c: command to run in shell
rem     trailing parameters for command go last

rem Usage:
rem     Using cmd.exe, execute the following command:
rem         C:\> C:\opt\users\johnsra2\sandbox\os-agnostic-utils\scripts\startBashWSL.cmd -r wsl.exe C: -s /usr/bin/bash -t "Bash window title" -l /mnt/c/opt/users/johnsra2/sandbox/os-agnostic-utils/demo-log.txt -c /mnt/c/opt/users/johnsra2/sandbox/os-agnostic-utils/scripts/demo 'hello world' 10

rem Notes:
rem     running from the command line, strings in the arguments must be in ticks (e.g., 'hello world')
rem     running from Python, strings in the arguments must be in escaped-ticks (e.g., '\'hello world\'')

rem Use EnableDelayedExpansion for generating array-like constructs
setlocal EnableDelayedExpansion

rem Capture all the arguments into an array-like construct
set ALL_ARGS=""
set /A i=0
for %%x in (%*) do (
  set /A i=!i!+1
  set ALL_ARGS[!i!]=%%x
)
set /A lastidx=!i!
rem echo There are !lastidx! variables present

rem example method for iterating through the arguments array
for /L %%f in (1,1,!lastidx!) do (
  echo %%f:!ALL_ARGS[%%f]!
)

rem Variables for required settings
set shell=""
set terminal=""
set bashcmd=""

rem Variables for optional settings
set title=""
set logfile=""
set bg=False

rem Collect the required and optional settings
rem fi is a variable for %%f+1
set /A fi=1
for /L %%f in (1,1,!lastidx!) do (
  set /A fi=!fi!+1
  rem ignore the entry unless it starts with -
  if "!ALL_ARGS[%%f]:~0,1!" == "-" (
    rem echo %%f: !ALL_ARGS[%%f]!
    if "!ALL_ARGS[%%f]!" == "-t" ( 
	  set /A lastfi=!fi!
      call set title=%%ALL_ARGS[!lastfi!]%%
    ) else (
	  if "!ALL_ARGS[%%f]!" == "-l" ( 
	    set /A lastfi=!fi!
        call set logfile=%%ALL_ARGS[!lastfi!]%%
      ) else (
	    if "!ALL_ARGS[%%f]!" == "-b" ( 
	      set /A lastfi=!fi!
          call set bg=True
        ) else (
		  if "!ALL_ARGS[%%f]!" == "-r" ( 
		    set /A lastfi=!fi!
		    call set terminal=%%ALL_ARGS[!lastfi!]%%
		  ) else (			
		    if "!ALL_ARGS[%%f]!" == "-s" ( 
			  set /A lastfi=!fi!
		  	  call set shell=%%ALL_ARGS[!lastfi!]%%
		    ) else (						  
		    if "!ALL_ARGS[%%f]!" == "-c" ( 
	          set /A lastfi=!fi!
              call set bashcmd=%%ALL_ARGS[!lastfi!]%%
            ) else (			
			    rem It is probably a parameter 				
			  )
		    )
          )		
        )
	  )
	)
  ) else (
    rem the entry did not start with - and is probably an argument to bashcmd
	rem echo %%f: !ALL_ARGS[%%f]!
  )
)

rem Collect remaining command line arguments to be passed to bashcmd
set /A firstidx=!lastfi!+1
rem echo !firstidx!:!lastidx!
for /L %%f in (!firstidx!,1,!lastidx!) do ( 
  if "!firstidx!"=="!lastidx!" (
    set BASHCMD_ARGS=!ALL_ARGS[%%f]!
  ) else (
    if "%%f"=="!firstidx!" (
      set BASHCMD_ARGS=!ALL_ARGS[%%f]!
    ) else (
      rem add space after
      set BASHCMD_ARGS=!BASHCMD_ARGS! !ALL_ARGS[%%f]!
    )
  )
)

if "%terminal%"=="" (
  call :printhelp
  rem ERROR_INVALID_PARAMETER=87
  echo ERROR_INVALID_PARAMETER: -r missing
  exit /b 87
) else (
  if "%shell%"=="" (
    call :printhelp
    rem ERROR_INVALID_PARAMETER=87
    echo ERROR_INVALID_PARAMETER: -s missing
    exit /b 87
  ) else (
	if %bashcmd%=="" (
      call :printhelp
      rem ERROR_INVALID_PARAMETER=87
	  echo ERROR_INVALID_PARAMETER: -c missing
      rem exit /b 87	  
	)
  )
  )
)

rem set default title if not present
if %title%=="" (
  set title="Start bash"
)
rem set default bg mode if not present
if %bg%=="" (
  set bg=False
)
echo Using:
echo   shell=%shell%
echo   terminal=%terminal%
echo   bashcmd=%bashcmd%
echo   args=!BASHCMD_ARGS!
echo   title=%title%
echo   bg=%bg%

rem remove quotes from title (if they are present)
for /f "useback tokens=*" %%a in ('%title%') do set title=%%~a

:startbash
echo ..................................................................
rem See the usage at bottom of this file for escape character rules
if %logfile%=="" (
  if %bg%==False (
    echo start "%title%" %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
    echo ..................................................................
    start "%title%" %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
  ) else (
    echo start "%title%" /B %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
    echo ..................................................................
    start "%title%" /B %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS!"
  )
) else (
  if %bg%==False (
    echo start "%title%" %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
    echo ..................................................................
    start "%title%" %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
  ) else (
    echo start "%title%" /B %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
    echo ..................................................................
    start "%title%" /B %terminal% %shell% --login -c "%bashcmd% !BASHCMD_ARGS! 2^>^&1 | tee -a %logfile%"
  )
)
echo ..................................................................
exit /b %ERRORLEVEL%

:printhelp
echo Usage:
echo     %~1 [options] [parameters for cmd]
echo.
echo Required settings:
echo	 -r^=^<TERMINAL^>		Full path to terminal
echo     -s^=^<BASH^>          Full posix path to bash
echo     -c^=^<CMD^>            Full posix path for command to execute in bash
echo.
echo Options:
echo     --help                 Display this help and exit
echo     -t^=^<TITLE^>        Title for the command prompt window invoked (if --bg is absent)
echo     -l^=^<LOG^>            Full posix path for logging stdout and stderr
echo     -b                   Run in background (i.e., no window visible)
echo.
echo All parameters following valid options are passed as parameters to CMD.
echo.
echo E.g., C:\^> startBash.cmd -t "Hello" -l /mnt/C/mylogfile.txt \
echo            -r "wsl -d Ubuntu" -s /usr/bin/bash -c /mnt/C/myscript arg1 arg2 arg3
echo would run the following from a Windows command shell:
echo       C:\^> start "Hello" wsl -d Ubuntu /usr/bin/bash --login -c "/c/myscript arg1 arg2 arg3 2>&1 | tee -a /C/mylogfile.txt"
echo.
echo In the case -b is set, /B would be added as an option to the start command.
echo.
echo Cautions to consider:
echo Parameters for cmd must not conflict with names of any required or optional parameters.
echo Parameters for cmd with command file special characters (for the Windows command shell) 
echo   present must be escaped.  E.g., per https://www.robvanderwoude.com/escapechars.php, the 
echo   following escapes are necessary:
echo     %% must be escaped via "%%%%"
echo     ^^ must be escaped via "^^"
echo     ^& must be escaped via "^&"
echo     ^< and ^> must be escaped via "^<" and "^>"
echo     ^| must be escaped via "^|"
echo     ^^! must be escaped via "^^!"
exit /b 0

endlocal

