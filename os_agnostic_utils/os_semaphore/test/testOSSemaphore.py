import unittest,time,os,sys

from os_agnostic_utils.os_semaphore.OSSemaphore import OSSemaphore
from os_agnostic_utils.os_semaphore.OSSemaphore import Role
from os_agnostic_utils import os_type

class testOSSemaphore(unittest.TestCase):
    def test(self):
        #Description: runs unit tests for OSSemaphore by creating CREATOR and USER
        #  instances; calls setup() on each; calls acquire() on each and attempts to
        #  to acquire one more than is available; calls release() on each and
        #  attempts to release after max are available; calls teardown() on each.
        #Returns: None        

        try:            
            print('Running unitTest() for OSSemaphore')      
            initialVal=2
            maxVal=2
            name='osSemaphore'
            print('\tTest constructor')      
            creator=OSSemaphore.static__init__(Role.CREATOR)
            user=OSSemaphore.static__init__(Role.USER)
            print('\tTest setup()')      
            creator.setup(name,initialVal,maxVal)
            user.setup(name)#this is blocking, call creator.setup() first
            print('\tTest acquire()')   
            if creator.acquire(1):
                print('\tCreator acquired semaphore')
            else:
                raise
            if user.acquire(1):
                print('\tUser acquired semaphore')
            else:
                raise
            if not creator.acquire(1):
                print('\tCreator acquire timeout')
            else:
                raise
            print('\tTest release()')      
            value=creator.release()
            print('\tCreator release value=%s' % (str(value)))
            value=user.release()
            print('\tUser release value=%s' % (str(value)))
            value=creator.release()
            
            if os_type.osType()==os_type.OsType.WINDOWS:
                if value==-1:
                    print('\tCreator release unsuccessful (semaphore value at max)')
                else:
                    raise
            else:
                if value==maxVal:
                    print('\tCreator release successful (semaphore value at max)')
                else:
                    raise                    
            print('\tTest teardown')      
            user.teardown()
            creator.teardown()                
            print('unitTest() for OSSemaphore finished')
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise

if __name__ == "__main__":
    print('Running unitTest() for OSSemaphore')      
    unittest.main()
    print('unitTest() for OSSemaphore finished')