import abc,os,sys,time
from enum import IntEnum
from typing import Type
from builtins import staticmethod

import win32event,win32file,win32security,pywintypes

from os_agnostic_utils.os_semaphore import OSSemaphore
from os_agnostic_utils.os_semaphore import Status
from os_agnostic_utils.os_semaphore import Role

class WindowsOS(OSSemaphore):#Windows semaphores count down from max to zero
    #Child class for Windows semaphores

    cERROR_FILE_NOT_FOUND=2
    cERROR_TOO_MANY_POSTS=298
            
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    @property
    def value(self) -> int:
        #value-int for the current semaphore value (not accessible in Windows)
        return(-1)
    
    @property
    def handle(self):#not sure how to type check return of type pywintypes.HANDLE()
        return self.__handle    
    @handle.setter
    def handle(self, value):#not sure how to type check input of type pywintypes.HANDLE()
        self.__handle=value 
            
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    def acquire(self, timeout: float) -> bool:#seconds
        #Description: acquires a semaphore (decrements count) if available or times out
        #Returns: False on timeout and True on success        
        try:
            if self.status==Status.READY:
                result = win32event.WaitForSingleObject(self.handle, timeout*1000)#must convert to milli-seconds
                if result==win32event.WAIT_OBJECT_0:#above returns WAIT_OBJECT_0 if acquired (i.e., did not timeout)
                    return (True)
                elif result==win32event.WAIT_ABANDONED or result==win32event.WAIT_TIMEOUT:
                    return (False)
            else:
                raise
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise 
        
    def release(self) -> int:
        #Description: releases a semaphore (increments count) if not max
        #Returns: int for previous semaphore value or -1 if error               
        try:
            if self.status==Status.READY:
                addvalue=1
                retval=win32event.ReleaseSemaphore(self.handle,addvalue)
                return (retval)
            else:
                return (-1)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            if exc_obj.winerror==self.cERROR_TOO_MANY_POSTS:
                return (-1)
            else:
                print(exc_type, filename, exc_tb.tb_lineno)
                raise 

    def _construct(self, name: str, initialval: int, maxval: int):
        #Private method
        #Description: create the named semaphore object in the OS and sets its initial and maximum values; initializes 
        #  self.name and self.handle; sets self.status
        #Returns: None
        try:
            if self.status==Status.NULL and self.role==Role.CREATOR:
                self.name=name#will be created in /dev/shm/
                sa = win32security.SECURITY_ATTRIBUTES()
                sa.bInheritHandle = True
                self.handle=win32event.CreateSemaphore(sa, initialval, maxval, str(name))
                self.status=Status.READY
                message='\t\tSemaphore %s with role %s and initial value is -1 (not readable on Windows)' % (self.name,self.role.toStr())#in Windows we can't see value
                print(message)
            else:
                raise
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise

    def _destruct(self):
        #Private method
        #Description: removes the named semaphore object from the OS and sets its name to '' and status to Status.DESTROYED
        #Returns: None
        try:
            if self.status==Status.READY and self.role==Role.CREATOR:
#Windows automatically destroys the semaphore once all handles are closed
                win32file.CloseHandle(self.handle)
                self.handle=None
                self.name=''
                self.status=Status.DISCONNECTED
            elif self.status==Status.DISCONNECTED:
                pass
            else:
                raise
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
            
    def _open(self, name: str):
        #Private method
        #Description: open a handle to the named semaphore object in the OS and sets it to self.handle; updates self.status; blocking
        #Returns: None
        try:
            if self.status==Status.NULL and self.role==Role.USER:#this is blocking, call creator.setup() first
                self.name=name
                semaphoreExists=False
                while semaphoreExists==False:
                    try:
                        self.handle=win32event.OpenSemaphore(win32event.EVENT_ALL_ACCESS, False, str(name))
                        semaphoreExists=True
                    except:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        if exc_obj.winerror==self.cERROR_FILE_NOT_FOUND:
                            time.sleep(1)
                            pass                       
                #Specifies all possible access flags for the event object. 
                self.status=Status.READY
                message='\t\tSemaphore %s with role %s and initial value is -1 (not readable on Windows)' % (self.name,self.role.toStr())#in Windows we can't see value
                print(message)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise 
        
    def _close(self):
        #Private method
        #Description: close the handle to the named semaphore object in the OS; sets self.handle=None; 
        #  sets self.status=Status.DISCONNECTED
        #Returns: None
        try:
            if self.status==Status.READY and self.role==Role.USER and (not self.handle==None):
                win32file.CloseHandle(self.handle)
                self.handle=None
                self.name=''
                self.status=Status.DISCONNECTED
            elif self.status==Status.DISCONNECTED:
                pass
            else:
                raise
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
