#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: Utility class for named semaphores that are used to monitor a pool of some 
#  resource being accessed by multiple processes and perform synchronized updates for the
#  availability of this resource.  Presently supports both Windows (using pywin32 
#  extensions) and Linux (using the python posix_ipc package).  There should be one
#  CREATOR process and one or more USER processes.
#
#Requirements: 
#  For Windows, install pywin32 extensions.  For Linux, install posix_ipc.
#  Windows install using the command prompt executing with Administrator privileges)
#    C:\> pip3 install pywin32
#  Linux install from bash:
#    $ sudo pip3 install posix_ipc
#
#Usage: Call the constructor for the appropriate OS you are using and specify the USER
#  or CREATOR role.  Then, initialize by passing the initial (i.e., number representing 
#  the initial resources available) and the maximum (i.e., total resources available) 
#  semaphore counts to setup().  Each call to acquire, from USER or CREATOR, will 
#  either decrement the count (if current count is greater than zero), or Timeout and 
#  return a status specifying that all resources are presently in use.  Each call to 
#  release, from USER or CREATOR, will increment the count to make a resource available.
#  Call the teardown() operation to gracefully close and remove any OS artifacts.  
# 
#Unit tests:
#  To run from python shell
#  >>> import os_semaphore
#  >>> os_semaphore.unitTest()
#  To run in PyDev debugger, make sure you set debugging environment variables PYDEVD_USE_CYTHON=NO and 
#    PYDEVD_USE_FRAME_EVAL=NO.  Create a test program with the two commands listed above and run it in a debug session.
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

import abc,os,sys,time
from enum import IntEnum
from typing import Type
from builtins import staticmethod

from os_agnostic_utils import os_type,os_semaphore

class Role(IntEnum):
    #Enumerated type for the OSSemaphore roles
    CREATOR=1 #creator process, there should only be one of these
    USER=2 #user process, there should be one or more of these
    
    @staticmethod
    def fromStr(strvalue: str) -> "Role":
        #Description: Creates a dictionary of values (Role) and keys (strings that represent their corresponding name).
        #  Uses strvalue as a key into the dict to determine and return its associated Role.
        #Returns: Role associated with value
        
        roles = {}
        roles['CREATOR'] = Role.CREATOR
        roles['USER']  = Role.USER
        assert strvalue in roles, "%s not a recognized Role" % strvalue
        return roles[strvalue]
    
    def toStr(self) -> str:
        #Description: Converts the Role to a str
        #Returns: str associated with Role
        
        if self==Role.CREATOR:
            retVal='CREATOR'
        elif self==Role.USER:
            retVal='USER'
        else:
            raise
            
        return(retVal)
        
class Status(IntEnum):
    #Enumerated type for the OSSemaphore status
    NULL=0 #self has not called setup
    READY=1 #semaphore exists and has been connected to by self; it may be acquired or released
    DISCONNECTED=2 #self has called teardown
    
    @staticmethod 
    def fromStr(strvalue: str) -> "Status":
        #Description: Creates a dictionary of values (Status) and keys (strings that represent their corresponding name).
        #  Uses strvalue as a key into the dict to determine and return its associated Status.
        #Returns: Status associated with value
        
        statuses = {}
        statuses['NULL'] = Status.NULL
        statuses['READY']  = Status.READY
        statuses['DISCONNECTED']  = Status.DISCONNECTED
        assert strvalue in statuses, "%s not a recognized status" % strvalue
        return statuses[strvalue]  
    
    def toStr(self) -> str:
        #Description: Converts the Status to a str
        #Returns: str associated with Status

        if self==Status.NULL:
            retVal='NULL'
        elif self==Status.READY:
            retVal='READY'
        elif self==Status.DISCONNECTED:
            retVal='DISCONNECTED'
        else:
            raise
            
        return(retVal) 

class OSSemaphore(object):
    #Parent class      
    
    ##################################################################################################      
    #ABSTRACT PROPERTIES
    ##################################################################################################      
    @abc.abstractproperty
    def value(self):
        pass
    
    @abc.abstractproperty
    def handle(self):
        pass    
    
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    @property
    def role(self):
        #role-os_semaphore.Role CREATOR or USER
        return self.__role    
    @role.setter
    def role(self, value: Type[Role]):
        if isinstance(value,Role):
            self.__role=value
        else:
            raise TypeError("role must be Role type")  
        
    @property
    def name(self):
        #name-str name for the semaphore
        return self.__name    
    @name.setter
    def name(self, value: str):
        if isinstance(value,str):
            self.__name=value
        else:
            raise TypeError("name must be str type")  
    
    @property
    def status(self):
        #status-os_semaphore.Status for the semaphore
        return(self.__status)   
    @status.setter
    def status(self, value: Type[Status]):#ignore the undefined variable error in Eclipse
        if isinstance(value,Status):
            self.__status=value
        else:
            raise TypeError("status must be Status type")    
      
    ##################################################################################################      
    #ABSTRACT METHODS
    ##################################################################################################      
    @abc.abstractmethod
    def acquire(self):
        pass

    @abc.abstractmethod
    def release(self):
        pass
         
    @abc.abstractmethod 
    def _construct(self):
        pass
    
    @abc.abstractmethod
    def _destruct(self):
        pass
    
    @abc.abstractmethod
    def _open(self):
        pass
    
    @abc.abstractmethod 
    def _close(self):
        pass
    
    ##################################################################################################      
    #METHODS
    ##################################################################################################     
    @staticmethod
    def static__init__(role: Type[Role]) -> "OSSemaphore":#resolve it later via using string with name        
        ostype=os_type.osType()
        if ostype==os_type.OsType.LINUX or ostype==os_type.OsType.MAC:
            from os_agnostic_utils.os_semaphore.PosixOS import PosixOS
            retval=PosixOS(role)
        elif ostype==os_type.OsType.WINDOWS:
            from os_agnostic_utils.os_semaphore.WindowsOS import WindowsOS
            retval=WindowsOS(role)
        return retval                       

    def __init__(self, role: Type[Role]) -> "OSSemaphore":#resolve it later via using string with name
        self.role=role
        self.handle=None
        self.name=''
        self.status=Status.NULL

    def setup(self, name: str, initialval: int=-1, maxval: int=-1):
        #Description: constructs or opens the named semaphore
        #Returns: None        
        try:
            if self.role==Role.CREATOR:
                if initialval==-1 or maxval==-1:
                    raise
                self._construct(name,initialval,maxval)  
            else:
                self._open(name)#this is blocking, call creator.setup() first
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise 
    
    def teardown(self):
        #Description: destructs or closes the named semaphore
        #Returns: None        
        try:
            if self.role==Role.CREATOR:
                self._destruct()  
            else:
                self._close()
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise 
