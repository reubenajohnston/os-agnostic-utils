#Description: Presence of __init__.py files instruct Python to treat directories containing the same
#  as packages.  In the simplest case, __init__.py can just be an empty file, but it can also execute
#  initialization code for the package or set the __all__ variable.  See the link below for more info.
#Reference: https://docs.python.org/3/tutorial/modules.html

#Created a package for Namespace name conflicts (e.g., Role and Status)
from os_agnostic_utils.os_semaphore.OSSemaphore import OSSemaphore
from os_agnostic_utils.os_semaphore.OSSemaphore import Role
from os_agnostic_utils.os_semaphore.OSSemaphore import Status
#the following have imports that are missing on non-native OS so import within a try/except statement
try:
    from os_semaphore.PosixOS import PosixOS
except ModuleNotFoundError:
    pass
try:
    from os_semaphore.WindowsOS import WindowsOS
except ModuleNotFoundError:
    pass

