import abc,os,sys,time,hashlib,posix_ipc,sysv_ipc
from enum import IntEnum
from typing import Type
from builtins import staticmethod

from os_agnostic_utils.os_semaphore import OSSemaphore
from os_agnostic_utils.os_semaphore import Status
from os_agnostic_utils.os_semaphore import Role

from os_agnostic_utils import os_type
   
OStype=os_type.osType()
if OStype==os_type.OsType.MAC:
    #For macOS, use sysv_ipc routines as sem_getvalue() and sem_timedwait() are not supported in macOS
    #https://www.ibm.com/developerworks/library/l-semaphore/index.html
    from sysv_ipc import Semaphore
else:
    from posix_ipc import Semaphore

class PosixOS(OSSemaphore):#posix semaphores count down from max to zero     
    #Child class for Posix semaphores (Linux or Mac OS)   
    
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################              
    @property
    def value(self):
        #value-int for the current semaphore value
        if self.status==Status.READY:
            return self.handle.value
        else:
            return(-1)
    
    @property
    def handle(self) -> Type[Semaphore]:
        #handle-posix_ipc.Semaphore object that interfaces with a posix named semaphore
        return self.__handle  
    @handle.setter
    def handle(self, value: Type[Semaphore]):        
        if isinstance(value,Semaphore):
            self.__handle=value
        elif isinstance(value,type(None)):
            self.__handle=value
        else:
            raise TypeError("handle must be posix_ipc.Semaphore type")       
        
    @property
    def key(self) -> Type[int]:
        #handle-posix_ipc.Semaphore object that interfaces with a posix named semaphore
        return self.__key  
    @key.setter
    def key(self, value: Type[int]):        
        if isinstance(value,int):
            if value>sysv_ipc.KEY_MAX:
                raise ValueError("key is too large")
            elif value<sysv_ipc.KEY_MIN:
                raise ValueError("key is too small")
            self.__key=value
        elif isinstance(value,type(None)):
            self.__key=value
        else:
            raise TypeError("handle must be int type")           

    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    def acquire(self, timeout: float) -> bool:#seconds
        #Description: acquires a semaphore (decrements count) if available or times out
        #Returns: False on timeout and True on success
        try:
            if self.status==Status.READY:
                OStype=os_type.osType()
                if OStype==os_type.OsType.MAC:
                    if self.handle.value>0:
                        result=self.handle.acquire()#blocking
                    else:
                        time.sleep(timeout)#macOS does not support timeouts, manually waiting
                        if self.handle.value>0:
                            result=self.handle.acquire()#blocking
                        else:
                            return(False)                                                
                else:
                    result=self.handle.acquire(timeout)#seconds
                if result==None:#above returns None if acquired (i.e., did not timeout)
                    return (True)
                else:
                    raise#shouldn't get here
            else:
                raise
        except posix_ipc.BusyError:
            return (False)#timeout condition
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
            
    def release(self) -> int:
        #Description: releases a semaphore (increments count) if not max
        #Returns: int for previous semaphore value or -1 if error
        try:
            if self.status==Status.READY:
                retval=self.value#use previous value to be consistent with Windows API
                self.handle.release()
                return(retval)
            else:
                return(-1)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise

    def _construct(self, name: str, initialval: int, maxval: int):
        #Private method
        #Description: create the named semaphore object in the OS and sets its initial and maximum values; initializes self.name and self.handle; sets self.status
        #Returns: None
        
        try:##will create in /dev/shm/sem.mcmcBayesSemaphore
            if self.status==Status.NULL and self.role==Role.CREATOR:                
                self.name=name
                OStype=os_type.osType()
                if OStype==os_type.OsType.MAC:
                    self.key=int(hashlib.md5(name.encode(encoding='utf_8', errors='strict')).hexdigest()[:14],16)
                else:
                    self.key=None
                
                if OStype==os_type.OsType.MAC:
                    self.handle=sysv_ipc.Semaphore(self.key, flags = sysv_ipc.IPC_CREX, initial_value=initialval)
                else:                    
                    posix_ipc.SEMAPHORE_VALUE_MAX=maxval
                    self.handle=posix_ipc.Semaphore(name, flags = posix_ipc.O_CREX, mode=0o0600, initial_value = initialval)   #for linux           
                self.status=Status.READY
            else:
                raise
        except (posix_ipc.ExistentialError, sysv_ipc.ExistentialError):
            message="\t\tSemaphore %s ExistentialError, try opening instead." % (name)
            print(message)
            
            while not self.status==Status.READY:
                try:
                    if OStype==os_type.OsType.MAC:
                        self.handle=sysv_ipc.Semaphore(self.key, flags = 0) #use flags=0 to open existing and don't set initial value here 
                    else:
                        self.handle=posix_ipc.Semaphore(self.name, flags = 0) #use flags=0 to open existing and don't set initial value here                   
                    self.status=Status.READY
                except (posix_ipc.ExistentialError, sysv_ipc.ExistentialError):
                    time.sleep(0.1)             

            while not self.value>=initialval:
                self.release()
                         
            message='\t\tSemaphore %s initial value is %d' % (self.name,self.value)
            print(message)      
            return      
            
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise     

    def _destruct(self):
        #Private method
        #Description: removes the named semaphore object from the OS and sets its name to '' and Status to Status.DESTROYED
        #Returns: None
        try:#one can also manually sudo delete the named semaphore in /dev/shm
            OStype=os_type.osType()
            if self.status==Status.READY and self.role==Role.CREATOR:
#TODO: check if any handles are open and raise if so
                if OStype==os_type.OsType.MAC:
                    self.handle.remove()
                else:
                    self.handle.unlink()
                self.handle=None
                self.name=''
            else:
                raise
        except (posix_ipc.ExistentialError,sysv_ipc.ExistentialError):
            self.handle=None
            self.name=''
            return
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise        

    def _open(self, name: str):
        #Private method
        #Description: open a handle to the named semaphore object in the OS and sets it to self.handle; updates self.status; blocking
        #Returns: None
        try:
            if self.status==Status.NULL:#need to allow both roles for construct() error handler
                self.name=name
                OStype=os_type.osType()
                if OStype==os_type.OsType.MAC:
                    self.key=int(hashlib.md5(name.encode(encoding='utf_8', errors='strict')).hexdigest()[:14],16)
                else:
                    self.key=None
                if self.status==Status.READY:
                    message='\t\tSemaphore already open and %s initial value is %d' % (self.name,self.value)
                else:
                    while not self.status==Status.READY:
                        try:
                            if OStype==os_type.OsType.MAC:
                                self.handle=sysv_ipc.Semaphore(self.key, flags = 0)
                            else:
                                self.handle=posix_ipc.Semaphore(name, flags = 0) #use flags=0 to open existing and don't set initial value here
                            self.status=Status.READY
                        except (posix_ipc.ExistentialError,sysv_ipc.ExistentialError):
                            time.sleep(0.1)                
                    message='\t\tSemaphore %s initial value is %d' % (self.name,self.value)
                    print(message)
            else:
                raise
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise  
    
    def _close(self):#posix_ipc.close()
        #Private method
        #Description: close the handle to the named semaphore object in the OS; sets self.handle=None; 
        #  sets self.status=Status.DISCONNECTED
        #Returns: None
        try:
            OStype=os_type.osType()
            if self.role==Role.CREATOR:
                if not(OStype==os_type.OsType.MAC):
                    self.handle.close()  
                self.status=Status.DISCONNECTED              
            else:
                if self.status==Status.READY:#needs to support both roles
                    if not(OStype==os_type.OsType.MAC):
                        self.handle.close()
                    self.handle=None
                    self.name=''
                    self.status=Status.DISCONNECTED
                else:
                    raise
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            pass