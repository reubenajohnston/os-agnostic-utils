'''
Created on Aug 13, 2019

@author: johnsra2
'''
import xml.etree.ElementTree as ET
import sys
import os

#Helper utility function that returns contents from xmlfile accessible as a list:
#E.g., xmlconfig[childtag]=grandchildren[], where grandchildren['grandchildtag']=grandchildtext

def parseXML(xmlfile): 
  
    try:
        # create element tree object 
        #print(ET)
        #print(xmlfile)
        tree = ET.parse(xmlfile) 
      
        # get root element 
        root = tree.getroot() 
      
        # create empty list 
        xmlList = {} 
      
        # iterate news items 
        #for child in root.getchildren():
        for child in list(root):       
            gcList = {}
            #print(child)
            #for grandChild in child.getchildren():  
            for grandChild in list(child):  
                #print(grandChild.tag+'='+grandChild.text)   
                gcList[grandChild.tag] = grandChild.text 
            xmlList[child.tag]=gcList
          
        return xmlList
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, exc_obj, filename, exc_tb.tb_lineno)