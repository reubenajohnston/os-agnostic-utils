import abc,ctypes,io,json,os,queue,subprocess,sys,threading,time,fcntl,termios
from enum import IntEnum
from typing import Type
from threading import Event
from io import FileIO

from os_agnostic_utils.os_pipe.OSPipe import OSPipe
from os_agnostic_utils.os_semaphore.OSSemaphore import OSSemaphore
from os_agnostic_utils.os_pipe.OSPipe import Status
from os_agnostic_utils.os_pipe.OSPipe import Role   

class PosixOS(OSPipe):
    #Child class for Posix named pipes (Linux or Mac OS)

    # Setup:
    #   Listener (server) calls os.mkfifo()
    #   Listener opens file for pipe (turn buffering off)
    #   Listener uses fcntl on file to change access to os.O_NONBLOCK
    #Listener check client connection
    #   Speaker (client) calls os.path.exists(self.name) until pipe exists   
    #   Speaker opens file for pipe (turn buffering off); pipe must exist
    #Speaker check server connection    
    # Operation:
    #   Listener thread monitors pipe using fcntl.ioctl(handle, termios.FIONREAD, bytes_avail), 
    #     if bytes are present, reads file to get them
    #   Speaker thread writes to pipe by writing to file
    # Teardown:
    #   Speaker calls win32file.FlushFileBuffers()
    #   Speaker calls CloseHandle(hPipe)
    #   Listener thread calls win32pipe.DisconnectNamedPipe()
    #   Listener thread calls win32file.CloseHandle()
    # Status queries:
    #   win32pipe.PeekNamedPipe() may be used to query available bytes in a pipe (by a listener or 
    #     speaker if r/w modes are appropriately used); must have an open handle to it
    #   sysinternals pipelist tool can be used to query pipe existence (e.g., $ pipelist | grep OSPipe)
    #   sysinternals handle tool can be used to query open file handles for a pipe (e.g., $ handle -nobanner -a OSPipe)
 
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    @property
    def handle(self):
        return self.__handle
    @handle.setter
    def handle(self, value: Type[FileIO]):       
        if isinstance(value,io.FileIO):
            self.__handle=value
        else:
            raise TypeError("handle must be io.BufferedRandom type")    

    @property
    def name(self):            
        #name-str path on the filesystem and name for the pipe        
        file=r'/tmp/OSPipe'+str(self.index)
        return (file)

    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    def check(self) -> bool:
        #Description: checks for existence of a client connection to a pipe (Role.LISTENER) or a pipe (Role.SPEAKER)
        #Returns: True if exists and False otherwise
        retVal=False
        try:
            if os.path.exists(self.name):
                if self.role==Role.LISTENER:
                    retVal=True #Linux always returns True
                else:
                    retVal=True #Linux always returns True
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise

        return(retVal)

    def _checkHandles(self) -> tuple:
        """ 
        Description: Queries the OS for any open file handles on the pipe; uses the r/w attributes to determine the corresponding role of the handle's owner
        """
        
        try:
            command=['lsof']
            proc=subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)  
            [stdoutdata,stderrdata]=proc.communicate()
        
            #For lsof process entries, there are no TID elements; since we are only interested in these entries, all the 
            #  element indices following TID are offset by -1.
            cidxCOMMAND=0
            cidxPID=1
            #cidxTID=2
            cidxUSER=2
            cidxFD=3
            cidxTYPE=4
            cidxDEVICE=5
            cidxSIZEOFF=6
            cidxNODE=7
            cidxNAME=8
            
            speaker=False
            listener=False
            
            for idx_line, line in enumerate(stdoutdata.splitlines()):
                #if idx_line==0:
                #    print(line)
                if self.name in line:
                    words=line.split()
                    if len(words)==9:#lsof process entries have no TID element
                        if 'u' in words[cidxFD] or 'w' in words[cidxFD]:
                            #print('speaker entry: '+line)
                            speaker=True
                        elif 'r' in words[cidxFD]:
                            #print('listener entry: '+line)      
                            listener=True
            
            return [speaker,listener]      
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise      

    def peek(self) -> int:
        #Description: checks for existence of message (for LISTENERs only) and returns size of available bytes
        #Returns: int for size of available bytes in pipe         
        retVal=0
        if self.role==Role.LISTENER:#what are the bytes available
            try:
                bytes_avail = ctypes.c_int()
                while not self.osSemaphore.acquire(self.cSEMAPHORETIMEOUT):
                    time.sleep(0.1)
                fcntl.ioctl(self.handle, termios.FIONREAD, bytes_avail)
                self.osSemaphore.release()
                retVal=bytes_avail.value#peek doesn't work so just return 1      
            except Exception:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, filename, exc_tb.tb_lineno)
                raise
        else:#Role.SPEAKER
            print("\t\tpeek(), not presently supported for Role.SPEAKER")    

        return(retVal)

    def speak(self, value: list):#is this blocking
        #Description: speaks (writes) the bytes in value to the named pipe
        #Returns: None        
        if self.status==Status.OPENED:
            try:
                if self.role==Role.LISTENER:
                    print("\t\tspeak(), not presently supported for Role.LISTENER")
                else:#Role.SPEAKER
                    while not self.osSemaphore.acquire(self.cSEMAPHORETIMEOUT):
                        time.sleep(0.1)
                    self.handle.write(value)
                    self.osSemaphore.release()
            except Exception:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, filename, exc_tb.tb_lineno)
                raise
        else:
            print("\t\tstatus must be Status.OPENED to speak to the pipe")                      

    def listen(self, size: int) -> list:#is this blocking
        #Description: listens (reads) size bytes from the named pipe
        #Returns: bytes read
        if self.status==Status.OPENED:
            try:
                if self.role==Role.LISTENER:
                    #listening everything available for linux
                    while not self.osSemaphore.acquire(self.cSEMAPHORETIMEOUT):
                        time.sleep(0.1)
                    msg=self.handle.read()# You won't listen anything until the sender executes close()
                    self.osSemaphore.release()
                else:#Role.SPEAKER
                    print("\t\tlisten(), not presently supported for Role.SPEAKER")
                    msg=None
            except Exception:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, filename, exc_tb.tb_lineno)
                raise
        else:
            print("\t\tstatus must be Status.OPENED to listen from the pipe")           
            msg=None
        return msg
              
    def _construct(self):
        #Private method
        #Description: create the named pipe object in the OS; initializes self.handle; sets self.status
        #Returns: None
        #
        #Linux does not gracefully close; pipes not shutdown appropriately.  Therefore, if you get this error:
        #  "OSError: [Errno 17] File exists", run os.remove(r'/tmp/mcmcBayes_pipe0') within python() interpreter
#TODO: maybe have a try/catch here for the condition above
        try:
            if self.role==Role.LISTENER:           
                if self.status==Status.UNKNOWN:        
                    try:   
                        if os.path.exists(self.name):
                            os.remove(self.name)#delete it if it exists, could be corrupted from unclean shutdown (same as os.unlink)
                        os.mkfifo(self.name,0o666)#create the fifo (does not open) and give all users r/w access
                        self.status=Status.CREATED
                    except FileExistsError:
                        self.status=Status.CREATED
                        pass
                    except Exception:
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, filename, exc_tb.tb_lineno)                
                        raise               
                else:
                    print("\t\tstatus must be Status.UNKNOWN to construct the pipe")
            else:
                print("\t\tOnly supporting construct() for Role.LISTENER")
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
        
    def _destruct(self):
        #Private method
        #Description: removes the named pipe object from the OS and sets its status to Status.DESTROYED
        #Returns: None
        if self.role==Role.LISTENER:           
            if self.status==Status.CLOSED or self.status==Status.CREATED:          
                try:
#should we block that no handles are open?
                
                    os.remove(self.name)
                    self.status=Status.DESTROYED
                except Exception:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, filename, exc_tb.tb_lineno)
                    self.status=Status.UNKNOWN
                    pass
            else:
                print("\t\tstatus must be Status.CLOSED or Status.CREATED to destruct the pipe (i.e., must have closed the pipe)")
        else:
            print("\t\tOnly supporting destruct() for Role.LISTENER")
   
    def _open(self):
        #Private method
        #Description: open a handle to the named pipe object in the OS and sets it to self.handle; updates self.status
        #Returns: None
        if self.status==Status.CREATED or self.status==Status.UNKNOWN:
            try:
                if self.role==Role.LISTENER:           
                    self.handle = open(self.name, 'rb', 0)#turn off buffering; reading only will block
                    fd=self.handle.fileno()#get the fd
                    flag = fcntl.fcntl(fd, fcntl.F_GETFD)#get current flags
                    fcntl.fcntl(fd,fcntl.F_SETFL, flag|os.O_NONBLOCK)#add non-blocking to current flags
                else:#Role.SPEAKER  
                    self.handle = open(self.name, 'wb', 0)#turn off buffering; writing only will block
                self.status=Status.OPENED
            except Exception:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, filename, exc_tb.tb_lineno)
                raise
        else:
            if not self.status==Status.OPENED:
                print("\t\tstatus must be Status.CREATED (for LISTENER) or Status.UNKNOWN (for SPEAKER) to open the pipe")           
   
        return(True)
    
    def _close(self):
        #Private method
        #Description: close the handle to the named pipe object in the OS; sets self.handle=None; 
        #  sets self.status=Status.DISCONNECTED
        #Returns: None
        try:
            if self.status==Status.OPENED:
                if self.role==Role.LISTENER:
                    self.handle.close()
                else:#Role.SPEAKER
                    self.handle.close()
                self.status=Status.CLOSED
            elif self.status==Status.DESTROYED:
                pass
            else:
                raise           
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            pass