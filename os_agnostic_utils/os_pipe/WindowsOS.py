import abc,ctypes,io,json,os,queue,subprocess,sys,threading,time
from enum import IntEnum
from typing import Type
from threading import Event
from io import FileIO

import win32file,win32pipe,pywintypes

from os_agnostic_utils.os_pipe.OSPipe import OSPipe
from os_agnostic_utils.os_semaphore.OSSemaphore import OSSemaphore
from os_agnostic_utils.os_pipe.OSPipe import Status
from os_agnostic_utils.os_pipe.OSPipe import Role   
    
class WindowsOS(OSPipe):
    #Description: Child class for Windows named pipes
    #
    # References: 
    #   https://docs.microsoft.com/en-us/windows/desktop/ipc/multithreaded-pipe-server
    #   https://docs.microsoft.com/en-us/windows/desktop/ipc/named-pipe-client
    # Setup:
    #   Listener (server) calls win32pipe.CreateNamedPipe() (creates and connects)
    #   Listener calls win32pipe.PeekNamedPipe() until the client connects (instead of using ConnectNamedPipe)
    #   Speaker (client) calls win32file.CreateFile(); pipe must exist
    #   Speaker calls win32pipe.PeekNamedPipe() until the server connects
    # Operation:
    #   Listener thread monitors pipe using win32pipe.PeekNamedPipe(), if bytes are present, calls
    #     win32pipe.ReadFile() to read them
    #   Speaker thread writes to pipe using win32file.WriteFile()
    # Teardown:
    #   Speaker calls win32file.FlushFileBuffers()
    #   Speaker calls CloseHandle(hPipe)
    #   Listener thread calls win32pipe.DisconnectNamedPipe()
    #   Listener thread calls win32file.CloseHandle()
    # Status queries:
    #   win32pipe.PeekNamedPipe() may be used to query available bytes in a pipe (by a listener or 
    #     speaker if r/w modes are appropriately used); must have an open handle to it
    #   sysinternals pipelist tool can be used to query pipe existence (e.g., $ pipelist | grep OSPipe)
    #   sysinternals handle tool can be used to query open file handles for a pipe (e.g., $ handle -nobanner -a OSPipe)
   
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    outbuffersize=65536
    inbuffersize=65536
    maxinstances=10                                            
    defaulttimeout=50
    securityattrib=None
    
    cERROR_FILE_NOT_FOUND=2
    cERROR_INVALID_HANDLE=6
    cERROR_BROKEN_PIPE=109
    cERROR_BAD_PIPE=230
    
    @property
    def handle(self):#not sure how to type check return of type pywintypes.HANDLE()
        return self.__handle    
    @handle.setter
    def handle(self, value):#not sure how to type check input of type pywintypes.HANDLE()
        self.__handle=value
      
    @property
    def name(self):            
        #name-str path on the filesystem and name for the pipe        
        file=r'\\.\pipe\OSPipe'+str(self.index)#Windows pipes must be located in '\\.\pipe\'
        return (file)
              
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    def check(self) -> bool:
        #Description: checks for existence of a client connection to a pipe (Role.LISTENER) or a pipe (Role.SPEAKER)
        #Returns: True if exists and False otherwise        
        try:
            (byteslisten, totalbytesavail, bytesleftinmsg)=win32pipe.PeekNamedPipe(self.handle,0)
            return(True)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            ExceptionType=exc_obj.args[0]                
            if ExceptionType==self.cERROR_BAD_PIPE:#pipe has not been connected to
                return(False)
            elif ExceptionType==self.cERROR_BROKEN_PIPE:#pipe has been closed
                return(False)
            elif ExceptionType==self.cERROR_INVALID_HANDLE:#pipe has not been opened
                return(False)
            else:
                filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, filename, exc_tb.tb_lineno)
                raise              

    def _checkHandles(self) -> tuple:
        """ 
        Description: Queries the OS for any open file handles on the pipe; uses the r/w attributes to determine the corresponding role of the handle's owner
        """
        
        try:
#TODO: handle lists the open handles but cannot determine their r/w mode; need to identify a utility that provides r/w 
#  mode for open fd (similar to lsof on posix)
            raise
            
            cidxPROCNAME=0
            cidxPID=2
            cidxTYPE=4
            cidxFD=5
            cidxFILENAME=6
            command=['C:\opt\sysinternals\handle.exe', '-nobanner', '-a', '-p', 'python.exe', 'OSPipe']
            #need -a to search pipes
            #-p python.exe narrows down to processes named python.exe
            proc=subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)  
            [stdoutdata,stderrdata]=proc.communicate()
            stdoutdata=stdoutdata.replace('\n\n','\n')
            for idx_line, line in enumerate(stdoutdata.splitlines()):
                print(line)
                words=line.split()
                strFD=words[cidxFD].replace(':','')
                FD=int(strFD, 16)
                print('\tfd='+str(FD))   
                strFILENAME=words[cidxFILENAME] 
             
                tupvar1=win32file.GetFileAttributesEx(strFILENAME)
                print("")         
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise      

    def peek(self) -> int:
        #Description: checks for existence of message (for LISTENERs only) and returns size of available bytes
        #Returns: int for size of available bytes in pipe (-1 if connection is not present)        
        retVal=0
        try:
            while not self.osSemaphore.acquire(self.cSEMAPHORETIMEOUT):
                time.sleep(0.1)
            (byteslisten, totalbytesavail, bytesleftinmsg)=win32pipe.PeekNamedPipe(self.handle,0)
            self.osSemaphore.release()
            retVal=bytesleftinmsg
            return(retVal)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            ExceptionType=exc_obj.args[0]
            self.osSemaphore.release()#need a release for self.cERROR_BAD_PIPE, self.cERROR_BROKEN_PIPE, self.cERROR_INVALID_HANDLE cases
            if ExceptionType==self.cERROR_BAD_PIPE:#pipe has not been connected to
                return(-1)
            elif ExceptionType==self.cERROR_BROKEN_PIPE:#pipe has been closed
                return(-1)
            elif ExceptionType==self.cERROR_INVALID_HANDLE:#pipe has not been opened
                return(-1)
            else:
                filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, filename, exc_tb.tb_lineno)
                raise       

    def speak(self, value: list):#is this blocking?
        #Description: speaks (writes) the bytes in value to the named pipe
        #Returns: None        
        try:
            if self.status==Status.OPENED:
                if self.role==Role.LISTENER:
                    raise#speak(), not presently supported for Role.LISTENER
                else:#Role.SPEAKER
                    while not self.osSemaphore.acquire(self.cSEMAPHORETIMEOUT):
                        time.sleep(0.1)
                    win32file.WriteFile(self.handle, value)
                    self.osSemaphore.release()                    
            else:
                raise#status must be Status.OPENED to speak to the pipe
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise

    def listen(self, size: int) -> list:#blocking!
        #Description: listens (reads) size bytes from the named pipe
        #Returns: bytes read
        try:
            if self.status==Status.OPENED:
                if self.role==Role.LISTENER:
                    while not self.osSemaphore.acquire(self.cSEMAPHORETIMEOUT):
                        time.sleep(0.1)
                    [hr,msg]=win32file.ReadFile(self.handle, size)#this blocks
                    self.osSemaphore.release()                    
                    return(msg)
                else:#Role.SPEAKER
                    raise#listen(), not presently supported for Role.SPEAKER"
            else:
                raise#status must be Status.OPENED to listen from the pipe    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
            
    def _construct(self):
        #Private method
        #Description: create the named pipe object in the OS; initializes self.handle; sets self.status
        #Returns: None
        try:
            if self.role==Role.LISTENER:           
                if self.status==Status.UNKNOWN:           
                    print('\t\tCreating named pipe ', self.name)
                    openmode=win32pipe.PIPE_ACCESS_DUPLEX                       
                    pipemode=win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_NOWAIT
                     
                    self.handle=win32pipe.CreateNamedPipe(self.name,openmode,pipemode,self.maxinstances,
                                                              self.outbuffersize,self.inbuffersize,self.defaulttimeout,
                                                              self.securityattrib)
                    win32file.FlushFileBuffers(self.handle)
                    self.status=Status.CREATED
                else:
                    raise#status must be Status.UNKNOWN to construct the pipe
            else:
                raise#only supporting construct() for Role.LISTENER
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise

    def _destruct(self):
        #Private method
        #Description: removes the named pipe object from the OS and sets its status to Status.DESTROYED
        #Returns: None
        try:
            #not sure if anything is necessary for Windows
            if self.role==Role.LISTENER:
                if self.status==Status.CLOSED:          
                    print('\t\tDestroying named pipe (does nothing for windows)', self.name)
                    self.status=Status.DESTROYED
                elif self.status==Status.DESTROYED:
                    pass
                else:
                    raise#status must be Status.CLOSED to destruct the pipe (i.e., must have closed the pipe)
            else:
                self.status=Status.DESTROYED
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            pass
            
    def _open(self) -> bool:#blocking for the Role.LISTENER role
        #Private method
        #Description: open a handle to the named pipe object in the OS and sets it to self.handle; updates self.status
        #Returns: True on success, False otherwise
        try:
            retVal=False
            if self.status==Status.CREATED or self.status==Status.UNKNOWN:
                if self.role==Role.LISTENER:           
                    self.status=Status.OPENED
                else:#Role.SPEAKER  
                    #pipe must exist or we'll get error  
                    print('\t\tCreating file for named pipe ', self.name)
                    self.handle = win32file.CreateFile(self.name,#fileName-PyUnicode string
                          win32file.GENERIC_READ | win32file.GENERIC_WRITE, 
                          #desiredAccess-device query attributes only-0, win32file.GENERIC_READ, win32file.GENERIC_WRITE
                          win32file.FILE_SHARE_READ,                        
                          #shareMode-None-0, FILE_SHARE_READ-1, FILE_SHARE_WRITE-2, FILE_SHARE_DELETE-4
                          None,#securityAttributes-None, PySECURITY_DESCRIPTOR
                          win32file.OPEN_EXISTING,                          
                          #creationDisposition-CREATE_NEW, CREATE_ALWAYS, OPEN_EXISTING, OPEN_ALWAYS, TRUNCATE_EXISTING
                          0,#fileAttributes-int 
                          None)#hTemplateFile-PyHANDLE
                    self.status=Status.OPENED#only gets here if successfully opened
                retVal=True
                return(retVal)               
            else:
                raise#status must be Status.CREATED to open the pipe
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            ExceptionType=exc_obj.args[0]                
            if ExceptionType==self.cERROR_FILE_NOT_FOUND: 
                print("\t\tWaiting for listener to construct, in _open()")
                #don't update self.status if failed to open
                return(retVal)
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise                  
           
    def _close(self):
        #Private method
        #Description: close the handle to the named pipe object in the OS; sets self.handle=None; 
        #  sets self.status=Status.DISCONNECTED
        #Returns: None
        try:
            if self.status==Status.OPENED:
                if self.role==Role.LISTENER:
                    print('\t\tDisconnecting (close) named pipe ', self.name)
                    win32pipe.DisconnectNamedPipe(self.handle)    
                    self.handle=None
                else:#Role.SPEAKER
                    print('\t\tClosing file for named pipe ', self.name)
                    win32file.CloseHandle(self.handle)
                    self.handle=None
                self.status=Status.CLOSED
            elif self.status==Status.DESTROYED:
                pass
            else:
                raise#status must be Status.OPENED to close the pipe      
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
