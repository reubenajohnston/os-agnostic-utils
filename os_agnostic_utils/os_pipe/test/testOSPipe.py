import unittest,time,os,sys,queue,threading,json
from threading import Event

from os_agnostic_utils.os_pipe.OSPipe import OSPipe
from os_agnostic_utils.os_pipe.OSPipe import Status
from os_agnostic_utils.os_pipe.OSPipe import Role

def run(testReady,testTerminate,testMsgQueue,Pipe):
    cTESTSLEEPINTERVAL=0.1#s
    #worker thread for unitTest()
    try:    
        print('\t\t'+Pipe.role.toStr()+' worker is starting')
        Pipe.setup()#blocking                 
        time.sleep(cTESTSLEEPINTERVAL)
        print('\t\t'+Pipe.role.toStr()+' worker is running')                                
        
        testReady.set()    

        while not testTerminate.is_set():
            time.sleep(cTESTSLEEPINTERVAL)
            if Pipe.role==Role.LISTENER:
                msglen=Pipe.peek()
                if msglen>0:
                    serializedmsg=Pipe.listen(msglen)
                    if not serializedmsg==None:
                        print('\t\t'+Pipe.role.toStr()+' worker writing to queue')                                                  
                        testMsgQueue.put(serializedmsg,block=False,timeout=None)
                        print('\t\t'+Pipe.role.toStr()+' worker wrote to queue')                                                  
#TODO: what if there are multiple messages in pipe?
            else:#Role.SPEAKER                
                if not testMsgQueue.empty():#poll queue for available message
                    print('\t\t'+Pipe.role.toStr()+' worker reading from queue')                                                  
                    serializedmsg=testMsgQueue.get(block=False,timeout=None)
                    print('\t\t'+Pipe.role.toStr()+' worker read from queue')                                                  
                    Pipe.speak(serializedmsg)#if present, read next from queue and write to pipe
                    
        testMsgQueue.queue.clear()#clear out the msgQueue
        
        print('\t\t'+Pipe.role.toStr()+' worker is exiting')
                  
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
        raise      
        
class testOSPipe(unittest.TestCase):    
    def test(self):
        try:
            print('Running unitTest() for OSPipe')      
            print('\tTest constructor for Role.LISTENER')      
            print('\tTest constructor for Role.SPEAKER')      
            pipeId=1
            pipeListener=OSPipe.static__init__(Role.LISTENER,pipeId)
            pipeSpeaker=OSPipe.static__init__(Role.SPEAKER,pipeId)

            listenerReady=Event()#Event that indicates the worker thread has started and self-initialized
            speakerReady=Event()#Event that indicates the worker thread has started and self-initialized
            listenerTerminate=Event()#Event that indicates the worker thread should shut itself down   
            speakerTerminate=Event()#Event that indicates the worker thread should shut itself down   
            listenerReady.clear()
            speakerReady.clear()            
            listenerTerminate.clear()
            speakerTerminate.clear()   
            listenerMsgQueue=queue.Queue(maxsize=0)
            speakerMsgQueue=queue.Queue(maxsize=0)
        
            listenerWorker=threading.Thread(target=run, name='listenerWorker', args=(listenerReady,listenerTerminate,listenerMsgQueue,pipeListener))#create the worker thread
            speakerWorker=threading.Thread(target=run, name='speakerWorker', args=(speakerReady,speakerTerminate,speakerMsgQueue,pipeSpeaker))#create the worker thread

            listenerWorker.start()
            speakerWorker.start()
            while not (listenerReady.is_set()):#ensure listener ready first (need to start them both before this because they block on each other)
                time.sleep(1)            
            while not (speakerReady.is_set()):
                time.sleep(1)                      
                        
            message1='hello reub 123.'
            sermsg1=json.dumps(message1).encode()
            print('\tSpeaker Write to queue: '+message1)
            speakerMsgQueue.put(sermsg1,block=False,timeout=None)                

            received=False
            while not received:
                if not listenerMsgQueue.empty():
                    serializedmsg=listenerMsgQueue.get(block=False,timeout=None)                
                    message2=json.loads(serializedmsg.decode())         
                    if isinstance(message2,str):
                        print('\tListener Read from queue: '+message2)                        
                        received=True
                time.sleep(1)

            if not (message1==message2):
                print('\tError, received message does not equal sent')
            else:
                print('\tReceived message equals sent')

            listenerTerminate.set()
            speakerTerminate.set()

            while not (listenerTerminate.is_set() and speakerTerminate.is_set()):
                time.sleep(1)            

            listenerWorker.join()
            speakerWorker.join()
            
            print('\tTest teardown() for Role.SPEAKER')      
            pipeSpeaker.teardown()
            print('\tTest teardown() for Role.LISTENER')      
            pipeListener.teardown()

            print('unitTest() for OSPipe finished')
        
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise        

if __name__ == "__main__":
    print('Running unitTest() for OSPipe')      
    unittest.main()
    print('unitTest() for OSPipe finished')