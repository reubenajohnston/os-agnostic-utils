#Description: Presence of __init__.py files instruct Python to treat directories containing the same
#  as packages.  In the simplest case, __init__.py can just be an empty file, but it can also execute
#  initialization code for the package or set the __all__ variable.  See the link below for more info.
#Reference: https://docs.python.org/3/tutorial/modules.html