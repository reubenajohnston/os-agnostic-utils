#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: Utility class for named pipes that are used to communicate between 
#  a parent process and its children.  Presently supports both Windows (using pywin32 
#  extensions) and Linux (using the python posix_ipc package).  There should be one
#  LISTENER process and one or more SPEAKER processes.
#
#Requirements: 
#  os_type.py, os_semaphore.py
#  For Windows, install pywin32 extensions.  For Linux, install posix_ipc.
#  Windows install using the command prompt executing with Administrator privileges)
#    C:\> pip3 install pywin32
#  Linux install from bash:
#    $ sudo pip3 install posix_ipc
#
#Usage:  Call the constructor for the appropriate OS you are using and specify the 
#  LISTENER or SPEAKER role and the index for the pipe.  Then, initialize by calling
#  setup() (Note, this is a blocking function).  Once the connections are established,
#  the speaker may write bytes to the fifo by calling speak() and the listener
#  should monitor the fifo using peek() to check for incoming messages and reading
#  bytes from the fifo by calling listen().  Call the teardown() operation to 
#  gracefully close and remove any OS artifacts. 
#
#Unit tests:
#  To run from python shell
#  >>> import os_pipe
#  >>> unitTest()
#  To run in PyDev debugger, make sure you set debugging environment variables 
#    PYDEVD_USE_CYTHON=NO and PYDEVD_USE_FRAME_EVAL=NO.  Create a test program with 
#    the two commands listed above and run it in a debug session.
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

import ctypes,io,json,os,queue,subprocess,sys,threading,time
from enum import IntEnum
from typing import Type
from threading import Event
from io import FileIO

from os_agnostic_utils.os_semaphore.OSSemaphore import OSSemaphore
from os_agnostic_utils import os_type,os_semaphore

class Role(IntEnum):
    #Enumerated type for the OSPipe roles
    LISTENER=1
    SPEAKER=2
    
    @staticmethod
    def fromStr(strvalue: str) -> "Role":
        #Description: Creates a dictionary of values (Role) and keys (strings that represent their corresponding name).
        #  Uses strvalue as a key into the dict to determine and return its associated Role.
        #Returns: Role associated with value
        
        roles = {}
        roles['LISTENER'] = Role.LISTENER
        roles['SPEAKER']  = Role.SPEAKER
        assert strvalue in roles, "%s not a recognized Role" % strvalue
        return roles[strvalue]
    
    def toStr(self) -> str:
        #Description: Converts the Role to a str
        #Returns: str associated with Role

        if self==Role.LISTENER:
            retVal='LISTENER'
        elif self==Role.SPEAKER:
            retVal='SPEAKER'
        else:
            raise
            
        return(retVal)

class Status(IntEnum):
    UNKNOWN=1
    CREATED=2
    OPENED=3
    CLOSED=4
    DESTROYED=5
    
    @staticmethod   
    def fromStr(strvalue: str) -> "Status":
        #Description: Creates a dictionary of values (Status) and keys (strings that represent their corresponding name).
        #  Uses strvalue as a key into the dict to determine and return its associated Status.
        #Returns: Status associated with value
        
        statuses = {}
        statuses['UNKNOWN'] = Status.UNKNOWN
        statuses['CREATED']  = Status.CREATED
        statuses['OPENED']  = Status.OPENED
        statuses['CLOSED']  = Status.CLOSED
        statuses['DESTROYED']  = Status.DESTROYED
        assert strvalue in statuses, "%s not a recognized Status" % strvalue
        return statuses[strvalue]
    
    def toStr(self) -> str:
        #Description: Converts the Status to a str
        #Returns: str associated with Status

        if self==Status.UNKNOWN:
            retVal='UNKNOWN'
        elif self==Status.CREATED:
            retVal='CREATED'
        elif self==Status.OPENED:
            retVal='OPENED'
        elif self==Status.CLOSED:
            retVal='CLOSED'
        elif self==Status.DESTROYED:
            retVal='DESTROYED'
        else:
            raise
            
        return(retVal)   

class OSPipe(object):
    #Parent class
    cSEMAPHOREMAXVAL=1
    cSEMAPHOREINITIALVAL=cSEMAPHOREMAXVAL
    cSEMAPHORETIMEOUT=5
    cSEMAPHOREBASENAME='accessSemaphoreOSPipe'    
         
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    @property
    def role(self) -> Type[Role]:
        #role-Role SPEAKER or LISTENER
        return(self.__role)   
    @role.setter
    def role(self, value: Type[Role]):
        if isinstance(value,Role):
            self.__role=value
        else:
            raise TypeError("role must be Role type")
        
    @property
    def index(self) -> int:
        #index-int index of the pipe
        return(self.__index)  
    @index.setter
    def index(self, value: int):
        if isinstance(value,int):
            self.__index=value
        else:
            raise TypeError("index must be int type")      
        
    @property
    def status(self) -> Type[Status]:
        #status-Status for the pipe
        return(self.__status)
    @status.setter
    def status(self, value: Type[Status]):#ignore the undefined variable error in Eclipse
        if isinstance(value,Status):
            self.__status=value
        else:
            raise TypeError("status must be Status type")
           
    @property
    def semaphoreName(self) -> str:
        #semaphoreName-name for the OSSemaphore
        return (self.cSEMAPHOREBASENAME+str(self.index))

    @property
    def osSemaphore(self) -> Type[OSSemaphore]:
        #osSemaphore-OSSemaphore for accessing the OSPipe
        return self.__osSemaphore   
    @osSemaphore.setter
    def osSemaphore(self, value: Type[OSSemaphore]):        
        if isinstance(value,OSSemaphore):
            self.__osSemaphore=value
        else:
            raise TypeError('osSemaphore must be os_semaphore.OSSemaphore type')
    
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    @staticmethod
    def static__init__(role: Type[Role], idx: int) -> "OSPipe":#resolve it later via using string with name        
        ostype=os_type.osType()
        if ostype==os_type.OsType.LINUX or ostype==os_type.OsType.MAC:
            from os_agnostic_utils.os_pipe.PosixOS import PosixOS
            retval=PosixOS(role,idx)
        elif ostype==os_type.OsType.WINDOWS:
            from os_agnostic_utils.os_pipe.WindowsOS import WindowsOS
            retval=WindowsOS(role,idx)
        return retval
            
    def __init__(self, role, idx) -> "OSPipe":
        #Description: initializes newly instantiated OSPipe(role,idx) objects by setting self.Role=setrole, self.index=idx and 
        #  self.status=Status.UNKNOWN
        #Returns: OSPipe object        
        
        self.role=role
        self.index=idx
        self.status=Status.UNKNOWN     
        
        try:
            if role==Role.LISTENER:
                self.osSemaphore=OSSemaphore.static__init__(os_semaphore.Role.CREATOR)
            else:
                self.osSemaphore=OSSemaphore.static__init__(os_semaphore.Role.USER)
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise              

    def setup(self):
        #Description: constructs or opens the named pipe; blocks
        #Returns: None
        try:
            if self.role==Role.LISTENER:
                print('\t\tListener calling semaphore setup()')
                self.osSemaphore.setup(self.semaphoreName,self.cSEMAPHOREINITIALVAL,self.cSEMAPHOREMAXVAL)
                print('\t\tListener calling pipe construct()')
                self._construct()
                while self.check()==False:#wait for speaker to connect
                    time.sleep(1)                
                print('\t\tListener calling pipe open()')
                self._open()#blocks
            else:
                print('\t\tSpeaker calling semaphore setup()')
                self.osSemaphore.setup(self.semaphoreName)
                
                print('\t\tSpeaker calling pipe open()')
                while not self._open():#wait for listener to construct
                    time.sleep(1)                 
                print('\t\tSpeaker waiting for listener to connect()')
                while self.check()==False:#wait for listener to connect
                    time.sleep(1)
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
    
    def teardown(self):
        #Description: destructs or closes the named pipe
        #Returns: None
        try:
            if self.role==Role.LISTENER:
                print('\t\tListener closing pipe')
                self._close()
#TODO: should the listener block waiting for all speakers to disconnect?
                print('\t\tListener destroying pipe')
                self._destruct()  
            else:
                print('\t\tSpeaker closing pipe')
                self._close()
                print('\t\tSpeaker destroying pipe')
                self._destruct()
            self.osSemaphore.teardown()
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
                                    
##################################################################################################      
    
