'''
Created on Jun 23, 2023

@author: johnsra2
'''
import os
import os_agnostic_utils
from os_agnostic_utils import os_type
from os_agnostic_utils import os_xml
   
def getConfig():
    dirname=os.path.dirname(os_agnostic_utils.__file__)
    cfgFile=dirname+os.sep+r'config.xml'
    
    cfgOsAgnosticUtils=os_xml.parseXML(cfgFile)
    OStype=os_type.osType()
    if OStype==os_type.OsType.WINDOWS:    
        shellStartScript=cfgOsAgnosticUtils['WindowsOs']['shellStartScript']         
        terminal=cfgOsAgnosticUtils['WindowsOs']['terminal']
        shell=cfgOsAgnosticUtils['WindowsOs']['shell']
        pythonscript=cfgOsAgnosticUtils['WindowsOs']['pythonscript']
        demo=cfgOsAgnosticUtils['WindowsOs']['demo']
        demolog=cfgOsAgnosticUtils['WindowsOs']['demolog']
        demomsgfile=cfgOsAgnosticUtils['WindowsOs']['demomsgfile']
    elif OStype==os_type.OsType.LINUX:
        shellStartScript=cfgOsAgnosticUtils['UbuntuOs']['shellStartScript']            
        terminal=cfgOsAgnosticUtils['UbuntuOs']['terminal']
        shell=cfgOsAgnosticUtils['UbuntuOs']['shell']
        pythonscript=cfgOsAgnosticUtils['UbuntuOs']['pythonscript']        
        demo=cfgOsAgnosticUtils['UbuntuOs']['demo']
        demolog=cfgOsAgnosticUtils['UbuntuOs']['demolog']
        demomsgfile=cfgOsAgnosticUtils['UbuntuOs']['demomsgfile']        
    elif OStype==os_type.OsType.MAC:
        shellStartScript=cfgOsAgnosticUtils['MacOs']['shellStartScript']         
        terminal=cfgOsAgnosticUtils['MacOs']['terminal']
        shell=cfgOsAgnosticUtils['MacOs']['shell']
        pythonscript=cfgOsAgnosticUtils['MacOs']['pythonscript']        
        demo=cfgOsAgnosticUtils['MacOs']['demo']
        demolog=cfgOsAgnosticUtils['MacOs']['demolog']
        demomsgfile=cfgOsAgnosticUtils['MacOs']['demomsgfile']        
    
    return (shellStartScript,terminal,shell,pythonscript,demo,demolog,demomsgfile) 