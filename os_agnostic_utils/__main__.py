#!/usr/bin/env python3
#Tested with vanilla Python 3.8
#
#Author: reuben@reubenjohnston.com
# 
#Description: 
#  See the readme.md for the top-level project.      
#
#Requirements: 
#  See the readme.md for the top-level project.
#
#Usage:
#  See the readme.md for the top-level project.
#
#Unit tests:
#  See the readme.md for the top-level project.
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601
#  2023-07-06 - Updated version validated by Reub on RHEL 9.2 and Windows 10 21H2

from enum import IntEnum
import argparse,sys,os,time,psutil

import os_agnostic_utils
from os_agnostic_utils import os_type
from os_agnostic_utils import os_shell
from os_agnostic_utils import os_files
from os_agnostic_utils import config
from os_agnostic_utils.os_processes.OSProcesses import OSProcesses

class CommandType(IntEnum):
    #Enumerated type for all the mcmcBayes commands
    #This needs to be located before main()
    RUNBASHCMD=1
    SPAWNBASHCMD=2
    QUERYBASHCMDSTATUS=3
    KILLPID=21
    KILLPIDS=22
    RUNMATLABENGINE=31
    SPAWNMATLABENGINES=32
    QUERYMATLABENGINESSTATUS=33      

    @staticmethod
    def fromStr(strvalue):
        #Creates a dictionary of values (CommandType) and keys (strings that represent their corresponding name).
        #Uses strvalue as a key into the dict to determine and return its associated CommandType.
        cmds = {}
        cmds['RUNBASHCMD'] = CommandType.RUNBASHCMD
        cmds['SPAWNBASHCMD'] = CommandType.SPAWNBASHCMD
        cmds['QUERYBASHCMDSTATUS'] = CommandType.QUERYBASHCMDSTATUS
        cmds['KILLPID']  = CommandType.KILLPID
        cmds['KILLPIDS'] = CommandType.KILLPIDS
        cmds['RUNMATLABENGINE'] = CommandType.RUNMATLABENGINE
        cmds['SPAWNMATLABENGINES'] = CommandType.SPAWNMATLABENGINES
        cmds['QUERYMATLABENGINESSTATUS'] = CommandType.QUERYMATLABENGINESSTATUS
        assert strvalue in cmds, "%s not a recognized mcmcbayes_utils_command_parser.CommandType" % strvalue
        return cmds[strvalue]
    
    def toStr(self):
        #Description: Converts the CommandType to a str
        #Returns: str associated with CommandType

        if self==CommandType.RUNBASHCMD:
            retVal='RUNBASHCMD'
        elif self==CommandType.SPAWNBASHCMD:
            retVal='SPAWNBASHCMD'
        elif self==CommandType.QUERYBASHCMDSTATUS:
            retVal='QUERYBASHCMDSTATUS'
        elif self==CommandType.KILLPID:
            retVal='KILLPID'
        elif self==CommandType.KILLPIDS:
            retVal='KILLPIDS'
        elif self==CommandType.RUNMATLABENGINE:
            retVal='RUNMATLABENGINE'            
        elif self==CommandType.SPAWNMATLABENGINES:
            retVal='SPAWNMATLABENGINES'            
        elif self==CommandType.QUERYMATLABENGINESSTATUS:
            retVal='QUERYMATLABENGINESSTATUS'            
        else:
            raise
            
        return(retVal)     
    
def parse():                
    try:
        OStype=os_type.osType()#check the OS type and instantiate the appropriate OSPipe    

        #Implement an ArgumentParser for these commands:
        descStr="""OS agnostic utility package"""
                
        parser=argparse.ArgumentParser(description=descStr,formatter_class=argparse.RawTextHelpFormatter,add_help=False)
        #Notes:
        #  nargs='?' one optional argument will be consumed from the command line (if present)
        #  nargs='+' gathers arguments up to the next -- separator into a list (also requires at least one argument)        
        #  Don't have a list (e.g., engineCommands) or optional argument (e.g., debug) right before positional arguments (i.e., 
        #    commandtype)
        parser.add_argument('commandtype', nargs='?', type=str, help='identifies the command to execute, must be type '+
                            'CommandType (RUNBASHCMD, SPAWNBASHCMD, QUERYBASHCMDSTATUS, CREATEFILE, REMOVEFILE, QUERYFILE, '+
                            'KILLPID, KILLPIDS, KILLNAMEDPROC, RUNMATLABENGINE, SPAWNMATLABENGINES, QUERYMATLABENGINESSTATUS, UNITTESTS)')    

        parser.add_argument('--msgFile', nargs='?', type=str, help='provide the full file-system path location for '+
                            'UUIDMsgFile (i.e., messaging interface between caller and os-agnostic-utils).')
        parser.add_argument('--help', '-h', action='store_true', help='show this help message and exit')
        parser.add_argument('--debug', action='store_true', help='specify this for extra debugging messages to the console')
        parser.add_argument('--shellTitle', nargs='?', type=str, help='provide the command window name for bash commands')
        parser.add_argument('--shellLog', nargs='?', type=str, help='provide the full file-system path location for the '+
                            'log file to create') 
        parser.add_argument('--shellRunView', nargs='?', type=str, help='specifies to run either in the foreground or background', 
                            default='mcmcBayes.t_runView.Foreground')
        parser.add_argument('--shellCmd', nargs='?', type=str, help='provide the command to run in bash')
        parser.add_argument('--shellArgs', nargs='+', type=str, help='list of shell arguments') #list
        parser.add_argument('--processPID', nargs='?', type=int, help='provide the operating system process identifier (PID)')
        parser.add_argument('--processParentPID', nargs='?', type=int, help='provide the operating system process identifier '+
                            '(PID) of the parent process')
        parser.add_argument('--processName', nargs='?', type=str, help='provide the operating system process name')    
        parser.add_argument('--fileName', nargs='?', type=str, help='provide the full file-system path location')
        
        #parser.add_argument('--killFile', nargs='?', type=str, help='provide the full file-system path location for the killFile')
        #parser.add_argument('--engineRunView', nargs='?', type=str, help='specifies to run the MATLAB engine command window either '+
        #                    'foreground or background', default='mcmcBayes.t_runView.Foreground')
        #parser.add_argument('--engineIndex', nargs='?', type=int, help='provide the index for the MATLAB engine in runMATLABEngine()')     
        #parser.add_argument('--engineCommands', nargs='+', type=str, help='list of matlab command line functions to execute in '+
        #                    'SPAWNMATLABENGINES') #list
        #parser.add_argument('--engineLogs', nargs='+', type=str, help='list of full-path log filenames for capturing the output '+
        #                    'from each MATLAB engine created by SPAWNMATLABENGINES')
        
        try:
            #print('ArgumentParser.parse_args() argv='+','.join(sys.argv))
            args=parser.parse_args()
        except SystemExit:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            message=('ArgumentParser.parse_args() error, argv=%s') %' '.join(sys.argv)
            print(message)
            message=('%s,%s,%s') % (exc_type, filename, exc_tb.tb_lineno)
            print(message)
            time.sleep(10)
            UsageAndExit(parser)
        
        if args.help:
            UsageAndExit(parser)
        
        if not args.commandtype==None:
            commandtypestr=args.commandtype
            commandtype=CommandType.fromStr(commandtypestr)            
        else:
            UsageAndExit(parser)
        
        debug=args.debug
        if debug:
            import pydevd
            print('Translating args to variables')         
        if not args.msgFile==None:
            msgFile=args.msgFile            
        if not args.shellTitle==None:
            shellTitle=args.shellTitle            
        if not args.shellLog==None:
            shellLog=args.shellLog
        shellRunView=args.shellRunView
        if not args.shellCmd==None:
            shellCmd=args.shellCmd   
        if not args.shellArgs==None: #list
            shellArgs=args.shellArgs                                   
        if not args.processPID==None:
            processPID=args.processPID
        if not args.processParentPID==None:
            processParentPID=args.processParentPID
        if not args.processName==None:
            processName=args.processName    
        if not args.fileName==None:
            fileName=args.fileName           
        
        shellStartScript,terminal,shell,demo,demolog,demomsgfile,pythonscript=config.getConfig()
        #shellStartScript,terminal,shell are used here
        #demo,demolog,demomsgfile,pythonscript are not used here (only used for unit tests)
        
        if debug:
            message=('Calling specified commandtype=%s') % (commandtypestr)
            print(message)
        if commandtype==CommandType.RUNBASHCMD or commandtype==CommandType.SPAWNBASHCMD:
            if not 'shellStartScript' in locals() and \
               not 'terminal' in locals() and \
               not 'shell' in locals():
                 print('Incorrect arguments for RUNBASHCMD or SPAWNBASHCMD')
                 UsageAndExit(parser)
            if not 'shellArgs' in locals():
                shellArgs=list()
            shellStartArgs=list()
            shellStartArgs.append('-r')
            shellStartArgs.append(terminal)
            shellStartArgs.append('-s')
            shellStartArgs.append(shell)
            if 'shellTitle' in locals():
                shellStartArgs.append('-t')
                shellStartArgs.append('"'+shellTitle+'"')
            if 'shellLog' in locals():
                shellStartArgs.append('-l')
                shellStartArgs.append(shellLog) 
            if shellRunView=='mcmcBayes.t_runView.Background':
                shellStartArgs.append('-b')
            if commandtype==CommandType.RUNBASHCMD:
                os_shell.runBashCommand(debug, msgFile, shellStartScript, shellStartArgs, shellCmd, shellArgs)
            else:
                os_shell.spawnBashCommand(debug, msgFile, shellStartScript, shellStartArgs, shellCmd, shellArgs)
        elif commandtype==CommandType.QUERYBASHCMDSTATUS:
            if not 'msgFile' in locals() and \
               not 'debug' in locals() and \
               not 'processPID' in locals():
                print('Incorrect arguments for QUERYBASHCMDSTATUS')
                UsageAndExit(parser)
            os_shell.queryBashCommand(debug, msgFile, processPID)            
        elif commandtype==CommandType.KILLPID:
            if not 'processPID' in locals():
                print('Incorrect arguments for KILLPID')
                UsageAndExit(parser)
            OSProcesses.killPID(processPID)
        elif commandtype==CommandType.KILLPIDS:
            if not 'processParentPID' in locals():
                print('Incorrect arguments for KILLPARENTPID')
                UsageAndExit(parser)
            OSProcesses.killPIDs(processParentPID)            
        else:
            UsageAndExit(parser)
    except SystemExit:
        pass #UsageAndExit calls sys.exit(), so ignore this exception in the try/except block to quit
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)

def UsageAndExit(parser):    
    parser.print_help()
    
    sys.exit()

def on_terminate(proc):
    print("process {} terminated with exit code {}".format(proc, proc.returncode))

if __name__=='__main__':
    parse()
    