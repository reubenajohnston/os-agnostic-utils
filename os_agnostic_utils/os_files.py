import os,sys
import subprocess

def queryFile(fileName):    
    #Returns True on exist, False otherwise
    retVal=False
    try:
        retVal=os.path.exists(fileName)
    except Exception:
        print("Exception, ", sys.exc_info()[0], ", in queryFile()")
    return(retVal)

def createFile(fileName):
    retVal=False
    try:
        subprocess.call(['touch', fileName])
    except Exception:
        print("Exception, ", sys.exc_info()[0], ", in createFile()")        
    return(retVal)

def removeFile(fileName):
    retVal=False
    try:
        subprocess.call(['rm', fileName])
    except Exception:
        print("Exception, ", sys.exc_info()[0], ", in removeFile()")        
    return(retVal)