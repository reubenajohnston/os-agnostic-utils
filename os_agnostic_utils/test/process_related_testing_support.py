#Requirements:
# setproctitle
# $ pip3 install setproctitle

cTIMEOUT=300

def sleepyProcess():
    import time,os,sys,setproctitle

    try:    
        curProcName=setproctitle.getproctitle()
        newProcName='sleepyProcess'
        message=('Renaming %s to %s') % (curProcName,newProcName)
        print(message)
        setproctitle.setproctitle(newProcName)
      
        pid=os.getpid()
        message=('\tpid%d is sleeping for %d seconds') % (pid,cTIMEOUT)
        print(message)
        
        for count in range(0,cTIMEOUT):
            time.sleep(1)
            message=('\t\tpid%d count is %d') % (pid,count)
            print(message)
            
        message=('\tpid%d completed!') % (pid)
        print(message)
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)    
        raise
    
def friskyProcess():
    import os,sys,setproctitle,time
    from os_agnostic_utils.os_processes.OSProcesses import OSProcesses

    try:
        curProcName=setproctitle.getproctitle()
        newProcName='friskyProcess'
        message=('Renaming %s to %s') % (curProcName,newProcName)
        print(message)
        setproctitle.setproctitle(newProcName)
        
        message=('Parent process pid=%d') % (os.getpid())
        print(message)
    
        pythonScript='process_related_testing_support.py'
        cmdId=1
        proc=list()
        for count in range(0,3):
            #proc.append(subprocess.Popen([pythonPath, pythonScript, '--pythonPath', pythonPath, '--cmdId', str(cmdId)]))      
            args=list()
            debug=True
            pid=OSProcesses.fork(debug,sleepyProcess,args)       
            message=('\tSpawned process pid=%d') % (pid)
            print(message)
            
        message=('Parent process now sleeping (so it does not become a zombie before we can kill it with the unittests)')
        print(message)
        time.sleep(300)
        
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)     
        raise
    
def doppleGangerProcess(newProcName):
    import os,sys,time,setproctitle
    #DOES NOT WORK ON WINDOWS
    
    try:
        curProcName=setproctitle.getproctitle()
        message=('Renaming %s to %s') % (curProcName,newProcName)
        print(message)
        setproctitle.setproctitle(newProcName)
        
        pid=os.getpid()
        message=('\tpid%d is sleeping for %d seconds') % (pid,cTIMEOUT)
        print(message)
        
        for count in range(0,60):
            time.sleep(1)
            message=('\t\tpid%d count is %d') % (pid,count)
            print(message)
            
        message=('\tpid%d completed!') % (pid)
        print(message)

    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)    
        raise