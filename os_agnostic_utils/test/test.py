#Debugging in Eclipse requires adding:
#   /${PROJECT_DIR_NAME} to Source folders in the project's Properties -> PyDev-PYTHONPATH 
#Debug configurations in Eclipse requires:
#   PYDEVD_USE_CYTHON=NO and PYDEVD_USE_FRAME_EVAL=NO in their environment variables
import unittest
from os_agnostic_utils.os_semaphore.test.testOSSemaphore import testOSSemaphore
from os_agnostic_utils.os_pipe.test.testOSPipe import testOSPipe
from os_agnostic_utils.os_processes.test.testOSProcesses import testOSProcesses

if __name__=='__main__':
    testmodules = [
        'os_agnostic_utils.os_semaphore.test.testOSSemaphore',
        'os_agnostic_utils.os_pipe.test.testOSPipe',
        'os_agnostic_utils.os_processes.test.testOSProcesses',
        'os_agnostic_utils.test.testOSPath',
        'testOSAgnosticUtils'
        ]
    #todo: 'os_comms.OSComms.unitTest'
    
    suite = unittest.TestSuite()
    
    for t in testmodules:
        try:
            # If the module defines a suite() function, call it to get the suite.
            mod = __import__(t, globals(), locals(), ['suite'])
            suitefn = getattr(mod, 'suite')
            suite.addTest(suitefn())
        except (ImportError, AttributeError):
            # else, just load all the test cases from the module.
            suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))
    
    unittest.TextTestRunner().run(suite)    

    