import unittest,time,os,sys,subprocess,runpy

from os_agnostic_utils import os_type
from os_agnostic_utils import config
from os_agnostic_utils import os_files
from os_agnostic_utils.os_processes.OSProcesses import OSProcesses
from os_agnostic_utils.test import process_related_testing_support

def altRun(module,argsDict):
    #updates sys.argv
    import os,sys
    
    sys.argv.clear()
    
    sys.argv.append(module)
    for idx,arg in enumerate(argsDict):
        sys.argv.insert(idx+1,arg)
        
    runpy.run_path(module,run_name='__main__')

class testOSAgnosticUtils(unittest.TestCase):
    #general
    shellStartScript=''
    terminal=''
    shell=''
    windowtitle=''
    demo=''
    demolog=''    
    demomsgfile=''    
    pythonscript=''

    cZZZ=60   
    cTIMEOUT=1
    cSLEEP=0.5    
    def setUp(self):     
        OStype=os_type.osType()
        
        self.shellStartScript,self.terminal,self.shell,self.pythonscript,self.demo,self.demolog,self.demomsgfile=config.getConfig()                

        if os.path.exists(self.demomsgfile):
            os.remove(self.demomsgfile)
        if os.path.exists(self.demolog):
            os.remove(self.demolog)

    def tearDown(self):
        pass
    
    def helperRunBashCmd(self, runview):
        command='RUNBASHCMD'
        shellargs=['\'Howdy ho!\'', '5']
        self.windowtitle='\"testRunBashCmd\"'        

        if os.path.exists(self.demomsgfile):
            os.remove(self.demomsgfile)
        if os.path.exists(self.demolog):
            os.remove(self.demolog)

        if ('mcmcBayes.t_runView.Foreground' in runview) or ('mcmcBayes.t_runView.Background' in runview):        
            altRun(self.pythonscript,[command,
                 '--msgFile', self.demomsgfile,
                 '--debug',
                 '--shellTitle', self.windowtitle,
                 '--shellRunView', runview,
                 '--shellLog', self.demolog,
                 '--shellCmd', self.demo,                                                     
                 '--shellArgs', shellargs[0], shellargs[1]])
            print('unitTest() for RunBashCmd finished')
        else:
            raise('Error, helperRunBashCmd() had a bad runview argument')
    
    def testRunBashCmd(self):
        #test os_shell.runBashCommand(debug, msgFile, shellStartScript, startShellArgs, shellCmd, shellArgs)

        print('Running unitTest() for RunBashCmd in Foreground')               
        self.helperRunBashCmd('mcmcBayes.t_runView.Foreground')

        print('Running unitTest() for RunBashCmd in Background')               
        self.helperRunBashCmd('mcmcBayes.t_runView.Background')


    def testSpawnAndQueryBashCmd(self):
        #test os_shell.spawnBashCommand(debug, msgFile, shellStartScript, startShellArgs, shellCmd, shellArgs)
        #test os_shell.queryBashCommand(debug, UUIDMsgFile, PID)
 
        print('Running unitTest() for SpawnBashCmd and QueryBashCmd')
        command='SPAWNBASHCMD'
        shellargs=['\'Howdy ho!\'', '30']        
        self.windowtitle='\"testSpawnAndQueryBashCmd\"'        
 
        if os.path.exists(self.demomsgfile):
            os.remove(self.demomsgfile)
        if os.path.exists(self.demolog):
            os.remove(self.demolog)        

        altRun(self.pythonscript,[command,
             '--msgFile', self.demomsgfile,
             '--debug',
             '--shellTitle', self.windowtitle,
             '--shellLog', self.demolog,
             '--shellCmd', self.demo,                                                     
             '--shellArgs', shellargs[0], shellargs[1]])                 
         
        while not os.path.exists(self.demomsgfile):
            time.sleep(0.1)
         
        #read the pid from UUIDMsgFile
        pid=-1
        while pid==-1:
            time.sleep(0.1)
            msgFilefd=open(self.demomsgfile,'r+')
            bufferlines=msgFilefd.read().splitlines() #read the file into buffer and split into lines
            msgFilefd.close()
            idx_pid=-1
            for index, line in enumerate(bufferlines): #loop across the buffer
                line=line.strip()
                bufferlines[index]=line
                if line=='##PID##': 
                    idx_pid=index+1 #save the index
            if idx_pid>=0:
                pid=int(bufferlines[idx_pid])
         
        message=('\t\tPID=%d') %(pid)
        print(message)
         
        #use QUERYBASHCMDSTATUS to query status and wait for completion
        finished=False
        while not finished:
            altRun(self.pythonscript,['QUERYBASHCMDSTATUS',
                             '--msgFile', self.demomsgfile,
                             '--debug',
                             '--processPID', str(pid)])                               
            msgFilefd=open(self.demomsgfile,'r+')
            bufferlines=msgFilefd.read().splitlines() #read the file into buffer and split into lines
            msgFilefd.close()
            for index, line in enumerate(bufferlines): #loop across the buffer
                line=line.strip()
                bufferlines[index]=line
                if line=='##STATE##': 
                    idx_state=index+1 #save the index
            state=bufferlines[idx_state]
            if state=='Finished' or state=='Error':
                finished=True       
            time.sleep(.5)            
         
        print('unitTest() for SpawnBashCmd and QueryBashCmd finished')             

    def testKillPid(self):
        #test killPID(killPID)    
         
        print('Running unitTest() for testKillPid') 
        command='KILLPID'
        args=list()
        pid=OSProcesses.fork(self.debug, process_related_testing_support.sleepyProcess, args)
        time.sleep(15)#short delay so we can verify in process monitor
        altRun(self.pythonscript,[command,
                                             '--debug',
                                             '--processPID', str(pid)])
        print('unitTest() for testKillPid finished')           
    def testKillPids(self):
        #test killPIDS(killParentPID)    
         
        print('Running unitTest() for testKillPids')
        command='KILLPIDS'
        args=list()
        parentPID=OSProcesses.fork(self.debug, process_related_testing_support.friskyProcess, args)
        time.sleep(15)#short delay so we can verify in process monitor
        altRun(self.pythonscript,[command,
                                                 '--debug',
                                                 '--processParentPID', str(parentPID)])     
        print('unitTest() for testKillPids finished')

if __name__ == "__main__":
    print('Running unitTest() for os_agnostic_utils')      
    unittest.main()
    print('unitTest() for os_agnostic_utils finished')  