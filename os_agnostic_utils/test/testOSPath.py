import unittest,time,os,sys

from os_agnostic_utils import os_type
from os_agnostic_utils import os_path

class testOSPath(unittest.TestCase):
    def test(self):
        try:            
            osType=os_type.osType()
            if osType==os_type.OsType.WINDOWS:
                unixPath=r'/C/opt'
                winPath=r'C:\opt'
                print('Unix to Windows path='+os_path.toWindows(unixPath))
                print('Windows to Windows path='+os_path.toWindows(winPath))
                print('Windows to Unix path='+os_path.toPosix(winPath))
                print('Unix to Unix path='+os_path.toPosix(unixPath))
            
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise
        
if __name__ == "__main__":
    print('Running unitTest() for os_path')      
    unittest.main()
    print('unitTest() for os_path finished')