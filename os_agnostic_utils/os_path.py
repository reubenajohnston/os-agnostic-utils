from os_agnostic_utils import os_type

def toPosix(path):
    #r'C:\sandbox' to r'/C/sandbox'
    #r'/C/sandbox' to r'/C/sandbox'
    retpath=r''
    if (path[0].isalnum() and path[1]==':'):
        retpath=r'/'+path[0]+'/'
        if len(path)>2:
            retpath=retpath+path[3:].replace(r'\\',r'/')
    else:
        assert(not(r'\\' in path))
        retpath=path
    return(retpath)
    
def toWindows(path):
    #r'/C/sandbox' to 'C:\sandbox'
    #r'C:\sandbox' to r'C:\sandbox'
    retpath=r''
    if path[0]==r'/':#posix path
        if (path[1].isalnum() and path[2]==r'/'):  
            retpath=path[1]+r':\\'
            if len(path)>2:
                retpath=retpath+path[3:].replace(r'/',r'\\')
    elif path[0].isalnum():#windows path
        assert(not(r'/' in path))
        retpath=path
    return(retpath)

#TODO toWSLWindows(path)