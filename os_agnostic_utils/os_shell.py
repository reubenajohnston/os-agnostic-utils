import os,sys,time,psutil
 
from os_agnostic_utils.os_processes.OSProcesses import OSProcesses

#Minimal requirements
#  1) run or fork a bash script
#  2) ability run bash script in foreground or background
#  3) ability to log stdout, stderr from bash script execution to a specified file
#  4) ability to customize window title when running in foreground
#  5) ability to query status of bash script (running, finished, failed) 
#  6) ability to force terminate bash script

#API
#  runBashCommand(debug, msgFile, shellStartScript, shellStartArgs, shellCmd, shellArgs)-wrapper for spawnBashCommand that will 
#    wait for the command to complete before exiting
#  spawnBashCommand(debug, msgFile, shellStartScript, shellStartArgs, shellCmd, shellArgs)-forks a process with the bash command
#    and then returns the pid for it
#  queryBashCommand(debug, msgFile, pid)-Checks whether the process identified by pid exists.  If so, the state remains "Running"
#    and this function returns;  otherwise, it will open the file designated by msgFile to update the ##STATE## and ##EXITCODE## 
#    lines properly.  In doing the latter, it reads in the entire file into a local buffer that is updated appropriately and then 
#    saved back into the file.

def runBashCommand(debug, msgFile, shellStartScript, shellStartArgs, shellCmd, shellArgs):   
    try:
        pid=spawnBashCommand(debug, msgFile, shellStartScript, shellStartArgs, shellCmd, shellArgs)

        finished=False
        while not finished:
            try:
                if debug:
                    message=('\t\tParent waiting for child pid=%d to finish') % (pid)
                    print(message)
                OSProcesses.poll(pid, 1)
                finished=True
            except TimeoutError:
                continue    

        if debug:
            message=('\t\tUpdating msgFile to Finished ##STATE##')
            print(message)
        queryBashCommand(debug, msgFile, pid)

        if debug:
            message=('\t\tChild pid=%d finished, parent exiting') % (pid)
            print(message)
 
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)                      

def spawnBashCommand(debug, msgFile, shellStartScript, shellStartArgs, shellCmd, shellArgs):   
    shellStartArgs.append('-c')
    shellStartArgs.append(shellCmd)
    
    for shellCmdArg in shellArgs:
        if not shellCmdArg=='':
            shellStartArgs.append(shellCmdArg)
    
    if debug:
        message=("\t\tspawnBashCommand is spawning worker process for %s") % (shellCmd)
        print(message)
    
    proclocatestr=shellCmd#for better readability
    pid=OSProcesses.forkIndirect(debug, shellStartScript, shellStartArgs, proclocatestr)

    psutilproc = psutil.Process(pid)# fetch the process info using PID
    procname = psutilproc.name()# query the process name
    proccmdline = psutilproc.cmdline()
            
    msgFilefd=open(msgFile,'w')#always create a new one

    message="##PID##\n%d\n" % (pid)
    msgFilefd.write(message)
    message="##PROCESSNAME##\n%s\n" % (procname)
    msgFilefd.write(message)
    message="##PROCESSCMDLINE##\n%s\n" % (' '.join(proccmdline))
    msgFilefd.write(message)
    message="##STATE##\n%s\n" % ('Running')
    msgFilefd.write(message)
    msgFilefd.close()

    if debug:
        message=('\t\tChild pid=%d is running, parent exiting') % (pid)
        print(message)
    
    return(pid)

def queryBashCommand(debug, msgFile, pid):  
    try:
        #Are we still running?    
        try:
            OSProcesses.poll(pid, 1)
        except TimeoutError:
            state="Running"

#TODO: add a simple error check that analyzes logfile for keywords ('deleting', 'error', 'stuck', 'inconsistent', others?)                      
#TODO: need to pass in logfile path
                
        #Update the msgFile if not running    
        if not 'state' in locals():
            fdMsgFile=open(msgFile,'r+')
            state="Finished"
            #this only updates file when the sampler process is not detected
            bufferlines=fdMsgFile.read().splitlines() #read the file into buffer and split into lines
            idx_state=-1
            if bufferlines:
                for index, line in enumerate(bufferlines): #loop across the buffer
                    line=line.strip()
                    bufferlines[index]=line
                    if line=='##STATE##': 
                        # when ##STATE## exists in buffer, replace it in the buffer (we can only have 
                        #   one STATE variable in the message file)
                        idx_state=index+1 #save the index
                    elif line=='##EXITCODE##':
                        tempstr=bufferlines[index+1] #read next value 
                        exitcode=int(tempstr) #convert XXX to an integer
                        if exitcode==0: #set state to Finished or Error
                            state="Finished"
                        else:
                            state="Error"
            if idx_state==-1: #append ##STATE##, 'State=Idle'
                bufferlines.append('##STATE##')
                bufferlines.append(state)
            else: #insert state at idx_state
                bufferlines[idx_state]=state
            fdMsgFile.seek(0,0) #reposition to beginning of file
            for line in bufferlines: #save the buffer
                fdMsgFile.write(line+'\n')
            fdMsgFile.close()
        if debug:
            print("\t\tstate=", state)
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
