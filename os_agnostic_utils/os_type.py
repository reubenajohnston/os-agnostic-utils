#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: Simple utility module to detect the OS type.
# 
#Requirements: 
#  None
#
#Usage:
#  To run from python shell
#  >>> import get_os
#  >>> get_os.osType()
#  >>> temp1=get_os.Type.fromStr('LINUX')
#  >>> print('temp1='+temp1.toStr())
#
#Unit tests:
#  None
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

import platform
from enum import IntEnum

class OsType(IntEnum):
#Enumerated type for the OS_pipeMessaging subclasses that implement the core functions for a particular OS
    WINDOWS=1
    LINUX=2
    MAC=3
    OTHER=4
    
    @staticmethod   
    def fromStr(strvalue):
        #Creates a dictionary of values (CommandType) and keys (strings that represent their corresponding name).
        #Uses strvalue as a key into the dict to determine and return its associated CommandType.
        types = {}
        types['WINDOWS'] = OsType.WINDOWS
        types['LINUX']  = OsType.LINUX
        types['MAC']  = OsType.MAC
        types['OTHER']  = OsType.OTHER
        assert strvalue in types, "%s not a recognized get_os.OsType" % strvalue
        return types[strvalue]
    
    def toStr(self):
        if self.value==OsType.WINDOWS:
            retVal='WINDOWS'
        elif self.value==OsType.LINUX:
            retVal='LINUX'
        elif self.value==OsType.MAC:
            retVal='MAC'
        elif self.value==OsType.OTHER:
            retVal='OTHER'
        else:
            raise ValueError
        
        return(retVal)    
    
def osType():
    querystr=platform.platform()
    if 'Windows' in querystr:
        retVal=OsType.WINDOWS
    elif 'Linux' in querystr:
        retVal=OsType.LINUX
    elif 'Darwin' in querystr:
        retVal=OsType.MAC
    else:
        retVal=OsType.OTHER
        print("%s is an unsupported OS", querystr)
    return(retVal)
