#!/usr/bin/env python3
#Tested with vanilla Python 3.6
#
#Author: reuben@reubenjohnston.com
# 
#Description: 
#
#Requirements: 
#  get_os.py, os_pipe.py, engine_comms_messaging.py
#
#Usage:   
#
#Unit tests:
#  To run from python shell
#  >>> import os_comms
#  >>> os_comms.unitTest()
#  To run in PyDev debugger, make sure you set debugging environment variables PYDEVD_USE_CYTHON=NO and PYDEVD_USE_FRAME_EVAL=NO.  
#    Create a test program with the two commands listed above and run it in a debug session.
#
#History:
#  2019-02-11 - Initial version validated by Reub on Ubuntu 18.04 and Windows 6.1.7601

import json,os,queue,sys,threading,time
from threading import Event
from os_agnostic_utils.os_pipe.OSPipe import OSPipe
from os_agnostic_utils.os_pipe.OSPipe import Role

class OSComms():       
    ##################################################################################################      
    #PROPERTIES
    ##################################################################################################      
    cTHREADBASENAME='osCommsWorker'
    cSLEEPINTERVAL=0.1#s
              
    ready=Event()#Event that indicates the worker thread should shut itself down
    terminate=Event()#Event that indicates the worker thread has started and self-initialized
                  
    @property
    def index(self):
        #index-index for the OSComms instance    
        return self.__index   
    @index.setter
    def index(self, value):
        if isinstance(value,int):
            self.__index=value
        else:
            raise TypeError('index must be int type')     
    
    @property
    def threadName(self):
        return (self.cTHREADBASENAME+str(self.index))
    
    @property
    def osPipe(self):
        #osPipe-OSPipe for communicating between processes
        return self.__osPipe   
    @osPipe.setter
    def osPipe(self, value):        
        if isinstance(value,OSPipe):
            self.__osPipe=value
        else:
            raise TypeError('osPipe must be os_pipe.OSPipe type')
            
    @property
    def msgQueue(self):
        #msgQueue-queue.Queue that contains messages for API functions communicating with the worker thread
        return self.__msgQueue    
    @msgQueue.setter
    def msgQueue(self,value):
        if isinstance(value,queue.Queue):
            self.__msgQueue=value
        else:
            raise TypeError('msgQueue must be queue type')
    
    @property
    def hWorker(self):
        #hWorker-threading.Thread that is the worker (listener or speaker)
        return self.__hWorker    
    @hWorker.setter
    def hWorker(self,value):
        if isinstance(value,threading.Thread):
            self.__hWorker=value
        else:
            raise TypeError("hWorker must be threading.Thread type")
 
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    @staticmethod
    def unitTest():
        #Unit tests for the OSComms class             
        try:
            print('Running unitTest() for OSComms')
            listener=OSComms(1,Role.LISTENER)
            speaker=OSComms(1,Role.SPEAKER)
            
            print('\tTesting setup()')
            listener.setup()
            speaker.setup()
            while not (listener.ready.is_set()):#ensure listener ready first (need to start them both before this because they block on each other)
                time.sleep(1)
            while not (speaker.ready.is_set()):
                time.sleep(1)
                
            print('\tTesting speak()')                        
            message1='hello reub 123.'
            sermsg1=json.dumps(message1).encode()                            
            speaker.speak(sermsg1)
            print('\t\tSent message')                        
            
            print('\tTesting listen()')                        
            received=False
            while not received:
                sermsg2=listener.listen()
                if not sermsg2==None:
                    message2=json.loads(sermsg2.decode())         
                    if isinstance(message2,str):
                        print('\t\tReceived message')                        
                        received=True
                time.sleep(1)
                
            if not (message1==message2):
                print('\tError, received message does not equal sent')
            else:
                print('\tReceived message equals sent')
            
            speaker.terminate.set()
            listener.terminate.set()
            
            while not (speaker.terminate.is_set() and listener.terminate.is_set()):
                time.sleep(1)            
            
            print('\tTesting teardown()')
            speaker.teardown()
            listener.teardown()
            print('unitTest() for OSComms finished')
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise            
       
    def __init__(self, index, role):
        #constructor for the OSComms class      
        try:
            self.index=index
            self.ready.clear()
            self.terminate.clear()
            
            self.osPipe=OSPipe.static__init__(role,index)             
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise            
    
    def setup(self):
        #startup routine for the OSComms class that is responsible for opening the communications pipe, 
        #  creating the worker thread, and starting the worker thread                
        try:                
            self.hWorker=threading.Thread(target=self.run, name=self.threadName, args=[])#create the worker thread
            self.hWorker.start()
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise            
                    
    def teardown(self):
        #shuts down the worker, closes the pipe, and destroys them (if listener)
        try:
            self.terminate.set()
            if not self.terminate.is_set():
                raise
            self.hWorker.join()#wait for the thread to terminate            
            if self.osPipe.role==Role.LISTENER:
                self.osPipe.teardown()
            else:#Role.SPEAKER
                self.osPipe.teardown()
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise  
    
    def speak(self, serializedmsg):
        #write msg to self.msgQueue; returns number of messages spoken (1 or 0)
        try:
            if self.ready.is_set():
                if not self.msgQueue.full():
                    self.msgQueue.put(serializedmsg,block=False,timeout=None)
                    return(1)#success
                else:
                    return(0)#failure               
            #else: we are initializing or shutting down
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise             
    
    def listen(self):
        #retrieves next message in self.msgQueue or returns None (when empty)
        try:
            if self.ready.is_set():
                if not self.msgQueue.empty():
                    serializedmsg=self.msgQueue.get(block=False,timeout=None)
                    return(serializedmsg)
                else:
                    return None
            #else: we are initializing or shutting down
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise   
    
    def run(self):
        #function for the worker thread that creates msgQueue and then either reads from it (to forward to pipe) or
        #  reads from the pipe (to forward to msgQueue)
        try:       
            self.msgQueue=queue.Queue(maxsize=0)
    
            #speaker or listener thread's worker function
            # will speak or listen to/from the named pipe on a timed interval and update msgQueue
            print('\t\t'+self.osPipe.role.toStr()+' worker is starting')
            
            self.osPipe.setup()#blocking           

            print('\t\t'+self.osPipe.role.toStr()+' worker osPipe is setup')

            time.sleep(self.cSLEEPINTERVAL)
            print('\t\t'+self.osPipe.role.toStr()+' worker is running')
            self.ready.set()    
                    
            while not self.terminate.is_set():
                time.sleep(self.cSLEEPINTERVAL)
                if self.osPipe.role==Role.LISTENER:
                    #poll pipe for available message
                    #if present, read from pipe and write to queue
                    msglen=self.osPipe.peek()
                    if msglen>0:
                        serializedmsg=self.osPipe.listen(msglen)
                        if not serializedmsg==None:
                            self.msgQueue.put(serializedmsg,block=False,timeout=None)
#TODO: what if there are multiple messages in pipe?
                else:#Role.SPEAKER                
                    if not self.msgQueue.empty():#poll queue for available message
                        serializedmsg=self.msgQueue.get(block=False,timeout=None)
                        self.osPipe.speak(serializedmsg)#if present, read next from queue and write to pipe
            #terminated
            self.msgQueue.queue.clear()#clear out the msgQueue
            print('\t\t'+self.osPipe.role.toStr()+' worker is exiting')
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise  
            