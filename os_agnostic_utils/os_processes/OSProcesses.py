import os,psutil,pymsgbox,sys,time,traceback

from os_agnostic_utils import os_type

class OSProcesses(object):
    cSTARTUPDELAY=3 #startup delay used by getpids that is indirectly called by forkIndirect
    
    ##################################################################################################      
    #ABSTRACT METHODS
    ##################################################################################################      
    @staticmethod
    def fork(debug, function, args):#forked child does not return; parent returns pid
        #Description: Traditional fork concept where the current process (parent) creates a new process (child), runs 
        #  Python function with args only in the child, and has the parent return the pid of the child.  On 
        #  PosixOS, uses os.fork() and on WindowsOS uses multiprocessing.Process (with spawn method).
        #Returns: pid of the new process
        try:
            OStype=os_type.osType()
            if OStype==os_type.OsType.WINDOWS:
                from os_agnostic_utils.os_processes.WindowsOS import WindowsOS
                pid=WindowsOS.fork(debug, function, args)   
            else:
                from os_agnostic_utils.os_processes.PosixOS import PosixOS
                pid=PosixOS.fork(debug, function, args)
             
            message=('child pid=%d') % (pid)
            print(message)         
            return(pid)
        except SystemExit:#gracefully close out the child
            sys.exit()        
    
    @staticmethod
    def forkIndirect(debug, command, args, procLocateStr):#forked child does not return; parent returns pid
        #Description: Alternate fork concept where the current process (parent) creates a new process (child), runs 
        #  an OS command with args only in the child, and has the parent return the pid of the child.  
        #  Uses subprocess.Popen for all instances.
        #Returns: pid of the new process
        #Notes:
        #  * Do not have a way to retrieve pid for commands that finish quickly
        #  * Coded assuming that child process's command includes a delay at startup that allows time for parent to query the child pid
        OStype=os_type.osType()
        if OStype==os_type.OsType.WINDOWS:
            from os_agnostic_utils.os_processes.WindowsOS import WindowsOS 
            pid=WindowsOS.forkIndirect(debug, command, args, procLocateStr)   
        else:
            from os_agnostic_utils.os_processes.PosixOS import PosixOS
            pid=PosixOS.forkIndirect(debug, command, args ,procLocateStr)
        if debug:
            message=('child pid=%d') % (pid)
            print(message)         
        return(pid)
    
    @staticmethod
    def poll(pid,timeout):
        #Description: Tests whether pid is still running.  Response is either raised TimeoutError after sleeping (pid exists and is running) or simply returns (pid does not exist and has finished)
        #Returns: None
        try:
            OStype=os_type.osType()
            if OStype==os_type.OsType.WINDOWS:   
                from os_agnostic_utils.os_processes.WindowsOS import WindowsOS 
                pid=WindowsOS.poll(pid,timeout)  
            else:
                from os_agnostic_utils.os_processes.PosixOS import PosixOS
                pid=PosixOS.poll(pid,timeout)
        except:
            raise              
        
    ##################################################################################################      
    #METHODS
    ##################################################################################################      
    @staticmethod
    def getpids(debug,procName):
        #Description: Finds all processes having procName in cmdline and retrieves their pids (necessary for locating specific spawned child of procName)
        #Returns: pids of the located processes
        try:
            if debug:
                message=('OSProcesses::getpids(%s,%s)') % (str(debug),procName)
                print(message)
                #traceback.print_stack()
            
            procs = psutil.process_iter()
            #procs = sorted(procs, key=str.lower proc: proc.name)#using lambda function sorts by date
            pids=[]
            OStype=os_type.osType()
            for proc in procs:
                try:
                    cmdline=proc.cmdline()
                    strcmdline=' '.join(cmdline)
                    if procName in strcmdline:
                        if debug:
                            message=('debug: %s located in pid(%d) with cmdline=%s') % (procName,proc.pid,strcmdline)
                            print(message) 
                        pids.append(proc.pid)                        
                except psutil.AccessDenied:
                    continue
                except psutil.ZombieProcess:
                    continue
                except SystemError:#Windows 10 raises this on some processes
                    continue
            if debug:
                if (len(pids)==0):
                    message=('Warning, OSProcesses::getpids(%s,%s) was unable to locate pid') % (str(debug),procName)
                    print(message)
            
            return(pids)        
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)            
    
    @staticmethod
    def killPID(pid):
        #Description: Kills the process with pid
        #Returns: None
        import sys,psutil,os
        
        #may need an OS mutex
        try:
            message=('\t\tKilling pid=%s') % (str(pid))
            print(message)
            p=psutil.Process(pid)
            p.kill()
        except psutil.NoSuchProcess:
            message=('psutil.NoSuchProcess exception, %s , in pid()') % (sys.exc_info()[0])
            print(message)        
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
                    
    @staticmethod
    def killPIDs(parentPid):
        #Description: Kills the process with parentPid and all its children.
        #Returns: None        
        import sys,psutil,os
        
        try:
            message=("\t\tKilling parentPID=%d and all subprocesses") % (parentPid)
            print(message)
            p=psutil.Process(parentPid)
            for subproc in p.children(recursive=True): #for some reason windows psutil requires using p.get_children()
                OSProcesses.killPID(subproc.pid)
            OSProcesses.killPID(parentPid)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            
    @staticmethod
    def killNamedProc(procName):
        #Description: Kills the process named procName.
        #DOES NOT WORK ON WINDOWS
        #Returns: None        
        import sys,psutil,os
        
        try:
            message=('\t\tKilling processes with name=%s') % (procName)
            print(message)
            for proc in psutil.process_iter(): #for some reason windows psutil requires using p.get_children()
                process = psutil.Process(proc.pid)# fetch the process info using PID
                pname = process.name()# query the process name
                if procName == pname:
                    OSProcesses.killPID(proc.pid)
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
    
    @staticmethod
    def psNameExists(procName):
        #Description: Determines whether a process named procName is running
        #Returns: True or False        
        import sys,psutil
        
        try:
            for proc in psutil.process_iter(): 
                process = psutil.Process(proc.pid)# fetch the process info using PID
                pname = process.name()# query the process procName
                if pname == procName:
                    print(procName," in ",pname)
                    return True
            return False
        except Exception:
            print("Exception, ", sys.exc_info()[0] , ", in psNameExists()")                
            return False    