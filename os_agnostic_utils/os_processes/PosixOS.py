import os, psutil, subprocess, sys, time

from os_agnostic_utils.os_processes.OSProcesses import OSProcesses

class PosixOS(object):  
    @staticmethod
    def fork(debug, function, args):#forked child does not return; parent returns pid
        try:
            pid=os.fork()
            strargs=''
            for arg in args:
                if not strargs=='':
                    strargs=strargs+', '+str(arg)
                else:
                    strargs=str(arg)
            
            command=('function(%s)') % (strargs)
            if pid==0:#child
                exec(command)
                os._exit(0)
            #parent
            if debug:
                message=('Created new child pid=%d, parent returning')  % (pid)
                print(message)
            return(pid)
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)  
            
    @staticmethod
    def forkIndirect(debug, command, args, proclocatestr):#forked child does not return; parent returns pid
        try:
            tcommand=list()
            tcommand.append(command)
            for arg in args:#append each shellArg in shellArgs list
                if not arg=='':
                    tcommand.append(arg)      
            popen=subprocess.Popen(tcommand)

            #On macOs, do not use popen.wait() as it pends completion of all the children (startbash script and anything else it spawns)
            #Wait for a small startup delay to ensure that the command is started and running in the child process
            #The command that is executed should also have a built-in startup delay so that it does not finish before the pid is identified

            time.sleep(OSProcesses.cSTARTUPDELAY)
            pids=OSProcesses.getpids(debug,proclocatestr)#this locates the grandchild's pid

            pid=None
            if (len(pids)>0):
                for tpid in pids:
                    try:
                        proc=psutil.Process(tpid)
                        cmdline=proc.cmdline()
                        strcmdline=' '.join(cmdline)
 #below may have issues with different posix os if bash is not first argument in cmdline; a solution would be to have this be queried from config.xml                            
                        if ('bash' in cmdline[0]):
                            pid=tpid
                            if debug:
                                message=('debug: forkIndirect identified pid=%d as the grandchild') % (pid)
                                print(message)
                    except psutil.NoSuchProcess:
                        pass

            if not pid==None:
                if debug: 
                    message=('Created new child pid=%d, parent returning')  % (pid)
            else:
                message=('Either child finished too quickly (maybe an error occurred in the command) or was not created')
                print(message)
                raise
            print(message)
            
            return(pid)
    
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            
    @staticmethod
    def poll(pid,timeout):#raises TimeoutError after sleeping (if exists, running) or returns (does not exist, finished)
        #use os.waitpid
        try:
            time.sleep(timeout)
    
            proc=psutil.Process(pid)
    
            if proc.is_running():
                if proc.status()=='zombie':
                    return
                else:
                    raise(TimeoutError)#still running
            #else, finished, just return
        except TimeoutError:
            raise
        except psutil.NoSuchProcess:
            return #finished, just return
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)  
