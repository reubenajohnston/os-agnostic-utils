import multiprocessing, os, psutil, sys, subprocess, time

from os_agnostic_utils.os_processes.OSProcesses import OSProcesses
from pickle import NONE

class WindowsOS(object): 
    @staticmethod
    def fork(debug, function, args):#forked child does not return; parent returns pid
        try:
            try:
                multiprocessing.set_start_method('spawn')#only supposed to call this once
            except RuntimeError:
                pass
            p=multiprocessing.Process(target=function,args=args)
            p.start()
            pid=p.pid #is None until spawned
            
            if debug:
                message=('Created new child pid=%d, parent returning')  % (pid)
                print(message)
            return(pid)
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)    
    
     
    @staticmethod
    def forkIndirect(debug, command, args, proclocatestr):#forked child does not return; parent returns pid
        try:
            tcommand=list()
            tcommand.append(command)
            for arg in args:#append each shellArg in shellArgs list
                if not arg=='':
                    tcommand.append(arg)      
            popen=subprocess.Popen(' '.join(tcommand))#Windows CreateProcess spawns a new process indirectly            
                   
            #On Windows, wait for the initial process to complete creation of its child
            popen.wait()
            
            #Wait for a small startup delay to ensure that the command is started and running in the child process
            #The command that is executed should also have a built-in startup delay so that it does not finish before the pid is identified
            time.sleep(OSProcesses.cSTARTUPDELAY)
            pids=OSProcesses.getpids(debug,proclocatestr)#this retrieves a list of pids that the grandchild process should be in

            #On Windows, find the pid of the grandchild with wsl (terminal) in its command line
            pid=None
            if (len(pids)>0):
                for tpid in pids:
                    try:
                        proc=psutil.Process(tpid)
                        cmdline=proc.cmdline()
                        strcmdline=' '.join(cmdline)
                        if ('wsl' in strcmdline):
                            pid=tpid
                            if debug:
                                message=('debug: forkIndirect identified pid=%d as the grandchild') % (pid)
                                print(message) 
                        
                    except psutil.NoSuchProcess:
                        pass

            if debug: 
                if not pid==None:
                    message=('Created new child pid=%d, parent returning')  % (pid)
                else:
                    message=('Either child finished too quickly (maybe an error occurred in the command) or was not created')
                    print(message)
                    raise
                print(message)
            
            return(pid)
    
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            
    @staticmethod
    def poll(pid,timeout):#raises TimeoutError after sleeping (if exists, running) or returns (does not exist, finished)
        #use os.waitpid
        try:
            time.sleep(timeout)
    
            proc=psutil.Process(pid)
    
            if proc==None:
                #finished, just return
                return
            else:
                if proc.is_running():
                    raise(TimeoutError)#still running
                else:
                    #finished, just return
                    return
            
        except TimeoutError:
            raise
        except psutil.NoSuchProcess:
            return #finished, just return
        except:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)  