import unittest,time,os,sys

from os_agnostic_utils import os_type
from os_agnostic_utils import config
from os_agnostic_utils.os_processes.OSProcesses import OSProcesses
from pickle import TRUE

def demo(message, sleepTime):
    try:
        #pymsgbox.alert(text=message, title='Demo', timeout=sleepTime*1000)
        #tkinter is not thread safe and pymsgbox will crash on MacOS when called from child
        message=('Child is running')
        print(message)
        message=('\tChild sleeping...')
        print(message)
        time.sleep(sleepTime)
        message=('\tChild woke up...')
        print(message)
    
    except Exception:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, filename, exc_tb.tb_lineno)
        raise

class testOSProcesses(unittest.TestCase):
    shellStartScript=''
    terminal=''
    shell=''
    windowtitle=''
    demo=''
    demolog=''
    demomsgfile=''
    pythonscript=''
    debug=True

    cZZZLong=120   
    cZZZ=10 
    cTIMEOUT=1
    cSLEEP=0.5    
    def setUp(self):     
        OStype=os_type.osType()
                
        self.shellStartScript,self.terminal,self.shell,self.pythonscript,self.demo,self.demolog,self.demomsgfile=config.getConfig()                
        self.windowtitle='\"Spawn demo\"'

    def tearDown(self):
        pass

    def testOSProcessesforkIndirect(self):
        try:             
            args=['-r', self.terminal,
                  '-s', self.shell,
                  '-t', self.windowtitle,
                  '-l', self.demolog, 
                  '-c', self.demo, 
                  '\'Howdy ho!\'', str(self.cZZZ)]#args 
            pid=OSProcesses.forkIndirect(self.debug,self.shellStartScript,args,self.demo)
                  
            running=True
            while running:
                try:
                    OSProcesses.poll(pid,self.cTIMEOUT)
                    running=False
                except TimeoutError:
                    time.sleep(self.cSLEEP)
                    print('\tParent waiting...')
                    continue
                  
            message=('Child finished')
            print(message)
 
        except SystemExit:
            sys.exit()
        
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise           

    def testOSProcessesFork(self):
        try:            
            pid=OSProcesses.fork(self.debug, demo,['\'Howdy ho!\'',self.cZZZ])
            running=True
            while running:
                try:
                    OSProcesses.poll(pid,self.cTIMEOUT)
                    running=False
                except TimeoutError:
                    time.sleep(self.cSLEEP)
                    print('\tParent waiting...')
                    continue
            message=('Child finished')
            print(message)
        
        except SystemExit:
            sys.exit()
        
        except Exception:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            filename = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, filename, exc_tb.tb_lineno)
            raise     

if __name__ == "__main__":
    print('Running unitTest() for OSProcesses')      
    unittest.main()
    print('unitTest() for OSProcesses finished')